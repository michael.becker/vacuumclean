/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client.route

import de.caterdev.vacuumclean.deebot.client.DeebotClient
import de.caterdev.vacuumclean.deebot.core.map.MapSetType
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post

fun Route.map(client: DeebotClient) {
    get("/reloadMapDataFromBot") {
        client.loadMapDataFromBot()

        call.respondRedirect("/")
    }
    get("/reloadMapDataFromFile") {
        client.loadMapDataFromFile()

        call.respondRedirect("/")

    }
    get("/saveMapDataToFile") {
        client.saveMapDataToFile()

        call.respondRedirect("/")
    }
    get("/displayMapUnscaled") {
        val width = "1000"
        val height = "1000"
        val data = client.activeRobot!!.cleanMap.mapInfo.unscaledHTMLCanvas

        val values = mapOf("width" to width, "height" to height, "mapData" to data, "title" to "Unscaled Map Data")

        call.respond(FreeMarkerContent("map/display_map.html", values))
    }
    get("/displayMapScaled") {
        val width = "800"
        val height = "600"
        val data = client.activeRobot!!.cleanMap.getScaledHTMLCanvas(800F, 600F)

        val values = mapOf("width" to width, "height" to height, "mapData" to data, "title" to "Scaled Map Data")

        call.respond(FreeMarkerContent("map/display_map.html", values))
    }

    get("/reloadSpotAreas") {
        client.loadSpotAreasFromBot()

        call.respondRedirect("/")
    }

    get("/displayMapScaledWithSpotAreas") {
        val width = "800"
        val height = "600"
        val data = client.activeRobot!!.cleanMap.getScaledHTMLCanvas(800F, 600F, true)

        val values = mapOf("width" to width, "height" to height, "mapData" to data, "title" to "Scaled Map Data with Spot Areas")

        call.respond(FreeMarkerContent("map/display_map.html", values))
    }

    get("/renameSpotAreas") {
        client.loadSpotAreasFromBot()

        val values = mapOf("spotAreas" to client.activeRobot!!.cleanMap.mapDetails[MapSetType.SpotArea], "mapSetId" to client.activeRobot!!.cleanMap.currentMapSetId[MapSetType.SpotArea])
        call.respond(FreeMarkerContent("map/rename_spot_areas.html", values))
    }
    post("/renameSpotAreas") {
        val parameters = call.receiveParameters()
        val mapSetNames = HashMap<String, String>()

        parameters.forEach { key, value ->
            mapSetNames[key] = value.joinToString()
        }

        client.renameSpotAreas(mapSetNames)
    }

    get("/clearMap") {
        client.clearMap()

        call.respondRedirect("/")
    }
}