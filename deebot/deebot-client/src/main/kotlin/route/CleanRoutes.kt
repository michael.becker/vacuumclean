/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client.route

import de.caterdev.vacuumclean.deebot.client.DeebotClient
import de.caterdev.vacuumclean.deebot.core.clean.CleanCommand
import de.caterdev.vacuumclean.deebot.core.constants.CleanAction
import de.caterdev.vacuumclean.deebot.core.constants.CleanSpeed
import de.caterdev.vacuumclean.deebot.core.constants.CleanType
import de.caterdev.vacuumclean.deebot.core.map.MapSetType
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post

fun Route.clean(client: DeebotClient) {
    get("/getCleanLogs") {
        val cleanLog = client.getCleanLogs()
        val values = mapOf("cleanedArea" to cleanLog.cleanedArea, "duration" to cleanLog.duration, "count" to cleanLog.count, "statistics" to cleanLog.cleanStatistics)

        call.respond(FreeMarkerContent("clean/display_clean_logs.html", values))
    }
    get("/clean") {
        val values = mapOf("cleanTypes" to CleanType.values(), "cleanSpeeds" to CleanSpeed.values(), "cleanActions" to CleanAction.values(), "spotAreas" to client.activeRobot!!.cleanMap.mapDetails[MapSetType.SpotArea])

        call.respond(FreeMarkerContent("clean/clean.html", values))
    }
    post("/clean/start") {
        val parameters = call.receiveParameters()
        val cleanType = CleanType.get(parameters["cleanType"])
        val cleanSpeed = CleanSpeed.get(parameters["cleanSpeed"])
        val cleanAction = CleanAction.get(parameters["cleanAction"])

        when (cleanType) {
            CleanType.SpotArea -> {
                val spotAreas = parameters["spotAreas"]!!.split(",").toTypedArray()
                client.clean(CleanCommand(cleanType, cleanSpeed, cleanAction, spotAreas))
            }
            CleanType.Auto -> {
                client.clean(CleanCommand(cleanType, cleanSpeed, cleanAction))
            }
        }


        println(parameters)

        val cleanCommand = CleanCommand(cleanType, cleanSpeed, cleanAction)
    }
    get("/startAutoClean") {}
    get("/startAutoClean/{speed}") {}
    get("/startSpotAreaClean/{spotAreaId}") {}
    get("/getTrace") {
        val traceInfo = client.loadTraceFromBot()
        client.activeRobot!!.cleanMap.updateScale(800F, 600F)

        val width = "800"
        val height = "600"
        val points = StringBuilder()

        points.append("[")
        traceInfo.points.forEach { point ->
            points.append("[").append(client.activeRobot!!.cleanMap.getPhoneX(point.positionX)).append(", ").append(client.activeRobot!!.cleanMap.getPhoneY(point.positionY)).append("], ")
        }
        points.append("];\n")

        val values = mapOf("width" to width, "height" to height, "positions" to points.toString())

        call.respond(FreeMarkerContent("clean/display_trace.html", values))
    }
}