/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client

import de.caterdev.vaccumclean.deebot.smack.packets.action.ActionPacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.ResponsePacket
import de.caterdev.vacuumclean.deebot.client.iqhandler.Deebot930IQManager
import de.caterdev.vacuumclean.deebot.client.iqhandler.Deebot930IQRequestHandler
import de.caterdev.vacuumclean.deebot.client.model.Bot
import de.caterdev.vacuumclean.deebot.core.DeviceTime
import de.caterdev.vacuumclean.deebot.core.clean.CleanCommand
import de.caterdev.vacuumclean.deebot.core.clean.CleanLog
import de.caterdev.vacuumclean.deebot.core.clean.CleanSchedule
import de.caterdev.vacuumclean.deebot.core.map.MapInfo
import de.caterdev.vacuumclean.deebot.core.map.MapSetType
import de.caterdev.vacuumclean.deebot.core.map.TraceInfo
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jxmpp.jid.Jid

class DeebotClient(val connection: XMPPTCPConnection) {
    private val sentPackets: MutableMap<String, ActionPacket<ResponsePacket>> = HashMap()
    val iqManager = Deebot930IQManager(connection, sentPackets)

    var cleanSchedules = emptyMap<String, CleanSchedule>()

    var robots: Map<String, Bot> = iqManager.getRoster()
    var activeRobot: Bot? = null
    var user: Jid = connection.user!!

    init {
        Thread(PingRunner(connection)).start()
    }

    fun setActiveRobot(key: String) {
        activeRobot = robots.getValue(key)
        connection.registerIQRequestHandler(Deebot930IQRequestHandler(connection, sentPackets, activeRobot!!))
    }

    suspend fun getCleanLogs(): CleanLog {
        val sumPacket = iqManager.getCleanSum(user, activeRobot!!.jid)
        val logPacket = iqManager.getCleanLogs(user, activeRobot!!.jid, 30)

        return CleanLog(sumPacket.cleandArea, sumPacket.duration, sumPacket.count, logPacket.cleanStatistics)
    }

    suspend fun getWaterPermeability(): String {
        val responsePacket = iqManager.getWaterPermeAbility(user, activeRobot!!.jid);

        return responsePacket.toString()
    }

    suspend fun getWaterBoxInfo(): String {
        val responsePacket = iqManager.getWaterBoxInfo(user, activeRobot!!.jid)

        return responsePacket.toString()
    }

    suspend fun loadMapDataFromBot(): String {
        val mapMResponse = iqManager.getMapModel(user, activeRobot!!.jid)

        activeRobot!!.cleanMap.mapInfo = MapInfo(mapMResponse.mapModel)

        for (i in 0 until mapMResponse.mapModel.crc.size) {
            val pullMResponse = iqManager.pullMapPiece(user, activeRobot!!.jid, i)

            if (activeRobot!!.cleanMap.mapInfo.getChecksum(i) != pullMResponse.mapPiece.checksum) {
                activeRobot!!.cleanMap.mapInfo.updateMapBuffer(pullMResponse.mapPiece)
            }
        }

        return "${mapMResponse.mapModel}, updated: ${activeRobot!!.cleanMap.mapInfo}, ${activeRobot!!.cleanMap}"
    }

    suspend fun loadMapDataFromFile(filename: String = "deebot.map") {
        val mapMResponse = iqManager.getMapModel(user, activeRobot!!.jid)
        activeRobot!!.cleanMap.mapInfo = MapInfo.readMapData(filename, mapMResponse.mapModel)
    }

    fun saveMapDataToFile(filename: String = "deebot.map") {
        activeRobot!!.cleanMap.mapInfo.writeMapData(filename)
    }

    suspend fun loadSpotAreasFromBot() {
        val mapSetResponse = iqManager.getMapSetType(user, activeRobot!!.jid, MapSetType.SpotArea)
        activeRobot!!.cleanMap.currentMapSetId[mapSetResponse.mapSetType] = mapSetResponse.mapSetId.toInt()
        activeRobot!!.cleanMap.mapDetails.clear()

        mapSetResponse.mapDetails.forEach { mapDetail ->
            val pullMResponse = iqManager.pullMapSet(user, activeRobot!!.jid, mapDetail.mapSetType, mapSetResponse.mapSetId.toInt(), mapDetail.mid, 0)

            mapDetail.setPositions(pullMResponse.positions)
            activeRobot!!.cleanMap.addMapDetail(mapDetail)
        }
    }

    suspend fun loadTraceFromBot(): TraceInfo {
        val traceModelResponse = iqManager.getTraceModel(user, activeRobot!!.jid)
        val traceInfoResponse = iqManager.getTraceInfo(user, activeRobot!!.jid, traceModelResponse.traceId, "0", (traceModelResponse.count.toInt() - 1).toString())

        val traceInfo = TraceInfo()
        traceInfo.updateTrace(traceModelResponse.traceId.toInt(), 0, traceModelResponse.count.toInt() - 1, traceInfoResponse.traceData)

        return traceInfo
    }

    suspend fun updateCleanSchedule(cleanSchedule: CleanSchedule) {
        if (this.cleanSchedules.containsKey(cleanSchedule.name)) {
            iqManager.modCleanSchedule(user, activeRobot!!.jid, cleanSchedule)
        } else {
            iqManager.addCleanSchedule(user, activeRobot!!.jid, cleanSchedule)
        }
        this.cleanSchedules = iqManager.getCleanSchedules(user, activeRobot!!.jid).cleanSchedulesMap
    }

    suspend fun deleteCleanSchedule(cleanScheduleName: String) {
        iqManager.deleteCleanSchedule(user, activeRobot!!.jid, cleanScheduleName)
        this.cleanSchedules = iqManager.getCleanSchedules(user, activeRobot!!.jid).cleanSchedulesMap
    }

    suspend fun getCleanSchedules(): List<CleanSchedule> {
        val responsePacket = iqManager.getCleanSchedules(user, activeRobot!!.jid)
        this.cleanSchedules = responsePacket.cleanSchedulesMap

        return responsePacket.cleanSchedules
    }

    suspend fun getTime(): DeviceTime {
        val responsePacket = iqManager.getTime(user, activeRobot!!.jid)

        return responsePacket.deviceTime
    }

    suspend fun charge() {
        iqManager.charge(user, activeRobot!!.jid)
    }

    suspend fun clean(cleanCommand: CleanCommand) {
        iqManager.clean(user, activeRobot!!.jid, cleanCommand.cleanType.type, cleanCommand.cleanSpeed.value, cleanCommand.cleanAction.value, cleanCommand.mids)
    }

    suspend fun renameSpotAreas(spotAreaNames: Map<String, String>) {
        iqManager.renameSpotAreas(user, activeRobot!!.jid, MapSetType.SpotArea, activeRobot!!.cleanMap.currentMapSetId[MapSetType.SpotArea]!!, spotAreaNames)
    }

    suspend fun clearMap() {
        iqManager.clearMap(user, activeRobot!!.jid)
    }

    suspend fun updateTime() {
        iqManager.updateTime(user, activeRobot!!.jid)
    }

    // TODO: autoreport an/ausstellen je nachdem, welcher Roboter ausgewählt wird
    suspend fun autoreport(autoreport: String) {
        while (true) {
            if (autoreport.equals("1")) {
                System.err.println("enabling autoreport from $activeRobot")
                iqManager.setMapAutoReport(user, activeRobot!!.jid, autoreport)
            } else {
                System.err.println("disabling autoreport from $activeRobot")
            }

            kotlinx.coroutines.delay(60000)
        }
    }
}