/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client

import de.caterdev.vaccumclean.deebot.smack.packets.Query
import de.caterdev.vacuumclean.core.ssl.VacuumCleanSSLContext
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.provider.ProviderManager
import org.jivesoftware.smack.roster.Roster
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import org.jxmpp.jid.parts.Resourcepart

class Deebot930ConnectionManager {
    private val APP_SERVER_DOMAIN = "ecouser.net";
    private val configurationBuilder = XMPPTCPConnectionConfiguration.builder()

    fun initConnection(host: String = "localhost", port: Int = 5223, username: String = "username23", password: String = "password", resource: String = "resource"): XMPPTCPConnection {
        configurationBuilder.setXmppDomain(APP_SERVER_DOMAIN)
        configurationBuilder.setHost(host)
        configurationBuilder.setPort(port)
        configurationBuilder.enableDefaultDebugger()
        configurationBuilder.setSendPresence(false)
        configurationBuilder.setCustomSSLContext(VacuumCleanSSLContext.get());

        Roster.setRosterLoadedAtLoginDefault(false)

        val connection = XMPPTCPConnection(configurationBuilder.build())
        ProviderManager.addIQProvider(Query.ELEMENT, Query.NAMESPACE, Query.Provider())

        connection.fromMode = XMPPConnection.FromMode.USER
        connection.connect()
        connection.login(username, password, Resourcepart.from(resource))

        return connection
    }
}