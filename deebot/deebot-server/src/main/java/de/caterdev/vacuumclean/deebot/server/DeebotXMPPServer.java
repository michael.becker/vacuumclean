/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.server;

import de.caterdev.vaccumclean.deebot.smack.packets.Query;
import de.caterdev.vacuumclean.server.NettyXMPPServer;
import org.jivesoftware.smack.provider.ProviderManager;

import java.net.InetSocketAddress;

public class DeebotXMPPServer extends NettyXMPPServer<DeebotXMPPClient> {
    public static final String SERVER_DOMAIN = "115.ecorobot.net";
    public static final String SERVER_DOMAIN2 = "de.ecorobot.net";
    public static final String APP_SERVER_DOMAIN = "ecouser.net";
    public static final int DEFAULT_PORT = 5223;

    public DeebotXMPPServer(InetSocketAddress address) throws Exception {
        super(new String[]{SERVER_DOMAIN, SERVER_DOMAIN2}, new String[]{APP_SERVER_DOMAIN}, address);

        ProviderManager.addIQProvider(Query.ELEMENT, Query.NAMESPACE, new Query.Provider());
    }

    @Override
    protected DeebotXMPPClient getClientInstance() {
        return new DeebotXMPPClient();
    }
}
