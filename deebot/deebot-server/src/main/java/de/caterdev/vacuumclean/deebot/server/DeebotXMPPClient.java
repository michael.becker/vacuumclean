/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.server;

import de.caterdev.vaccumclean.deebot.smack.packets.action.GetBatteryInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.action.GetDeviceInfoPacket;
import de.caterdev.vacuumclean.server.NettyXMPPClient;
import de.caterdev.vacuumclean.server.NettyXMPPServer;
import lombok.extern.java.Log;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Session;
import org.jivesoftware.smack.packet.UnparsedIQ;
import org.jivesoftware.smack.sasl.packet.SaslStreamElements;
import org.jxmpp.jid.impl.JidCreate;
import org.xmlpull.v1.XmlPullParser;

import java.util.Arrays;
import java.util.Base64;

@Log
public class DeebotXMPPClient extends NettyXMPPClient {
    public static final String LOG_MESSAGE_UNHANDLED_PRESENCE = "unhandled presence: %s\n";
    public static final String LOG_MESSAGE_AUTHDATA_ERROR = "could not extract username/password from auth data %s\n";
    public static final String LOG_MESSAGE_UNPARSABLE_IQ = "could not parse %s\n";

    public static final String DEEBOT_PRESENCE_STATUS_REQUEST = "hello world";
    public static final String DEEBOT_PRESENSE_STATUS_RESPONSE = "dummy";

    public DeebotXMPPClient() {
        super(DeebotXMPPServer.SERVER_DOMAIN, DeebotXMPPServer.APP_SERVER_DOMAIN);
    }

    protected void handlePresence(Presence presence) throws Exception {
        if (null != presence.getStatus() && presence.getStatus().equals(DEEBOT_PRESENCE_STATUS_REQUEST)) {   // Bot
            Presence response = new Presence(getClientJid(), Presence.Type.available);
            response.setFrom(JidCreate.from(DeebotXMPPServer.SERVER_DOMAIN));
            response.setStatus(DEEBOT_PRESENSE_STATUS_RESPONSE);

            send(response);
            send(new GetDeviceInfoPacket(JidCreate.from(DeebotXMPPServer.SERVER_DOMAIN), getClientJid()));
        } else {
            log.severe(String.format(LOG_MESSAGE_UNHANDLED_PRESENCE, presence.toXML(null).toString()));
        }
    }


    protected void handleAuthorisation(XmlPullParser parser) throws Exception {
        String data = parser.nextText();

        String[] authenticationData = new String(Base64.getDecoder().decode(data)).split("\\x00");
        if (3 == authenticationData.length) {
            setClientName(authenticationData[1]);
            System.out.println("++++++" + Arrays.asList(authenticationData));
        } else {
            log.severe(String.format(LOG_MESSAGE_AUTHDATA_ERROR, Arrays.asList(authenticationData)));
        }

        setAuthenticated(true);


        String command = new SaslStreamElements.Success(null).toXML(null).toString();

        send(command);
    }

    @Override
    protected <U extends NettyXMPPClient> void handleIQ(IQ iq, XmlPullParser parser, NettyXMPPServer<U> server) throws Exception {
        if (iq instanceof GetBatteryInfoPacket) {
            server.getRobotClient().send(iq);
        } else if (iq instanceof UnparsedIQ) {
            parser.nextTag();
            if (parser.getEventType() == XmlPullParser.START_TAG) {
                switch (parser.getName()) {
                    case Session.ELEMENT: {
                        handleResultExpected(iq);
                        break;
                    }
                    default: {
                        log.severe(String.format(LOG_MESSAGE_UNPARSABLE_IQ, parser.getName()));
                        break;
                    }
                }
            }
        }
    }
}
