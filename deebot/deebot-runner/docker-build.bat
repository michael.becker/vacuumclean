docker build -f dockerfile-amd64 -t caterdev/deebot-runner:amd64 .
docker build -f dockerfile-arm64 -t caterdev/deebot-runner:arm64 .

docker push caterdev/deebot-runner:amd64
docker push caterdev/deebot-runner:arm64