/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.response;

import de.caterdev.vaccumclean.deebot.smack.packets.response.GetMapModelResponsePacket;
import de.caterdev.vacuumclean.deebot.core.map.MapModel;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import java.util.zip.CRC32;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GetMapModelResponsePacketTest extends AbstractPacketTest {
    public static final int MAP_ID = 1;
    public static final int COLUMN_GRID = 5;
    public static final int COLUMN_PIECE = 3;
    public static final int ROW_GRID = 5;
    public static final int ROW_PIECE = 3;
    public static final int PIXEL_WIDTH = 10;
    public static final String CHECKSUMS;

    public static final String PACKET;

    static {
        StringBuilder builder = new StringBuilder();
        builder.append(getExpectedCRC32ValueForEmptyData(COLUMN_GRID * ROW_GRID));
        for (int i = 1; i < COLUMN_PIECE * ROW_PIECE; i++) {
            builder.append(",").append(getExpectedCRC32ValueForEmptyData(COLUMN_GRID * ROW_GRID));
        }

        CHECKSUMS = builder.toString();
        PACKET = String.format("<iq xmlns='jabber:client' to='%s' from='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl id='%s' p='%s' r='%s' c='%s' w='%s' h='%s' i='%s' m='%s'/></query></iq>", to, from, id, innerId, PIXEL_WIDTH, ROW_PIECE, COLUMN_PIECE, ROW_GRID, COLUMN_GRID, MAP_ID, CHECKSUMS);
    }

    private static long getExpectedCRC32ValueForEmptyData(int dataSize) {
        CRC32 crc32 = new CRC32();
        crc32.update(new byte[dataSize]);

        return crc32.getValue();
    }

    @Test
    void testParseSyntheticPacket() throws Exception {
        GetMapModelResponsePacket packet = PacketParserUtils.parseStanza(PACKET);
        assertEquals(id, packet.getStanzaId());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(from, packet.getFrom().asUnescapedString());

        MapModel mapModel = packet.getMapModel();
        assertNotNull(mapModel);
        assertEquals(MAP_ID, mapModel.getMapId());
        assertEquals(PIXEL_WIDTH, mapModel.getPixelWidth());
        assertEquals(COLUMN_GRID, mapModel.getColumnGrid());
        assertEquals(COLUMN_PIECE, mapModel.getColumnPiece());
        assertEquals(ROW_GRID, mapModel.getRowGrid());
        assertEquals(ROW_PIECE, mapModel.getRowPiece());
        assertEquals(COLUMN_PIECE * ROW_PIECE, mapModel.getCrc().length);
        assertEquals(COLUMN_GRID * ROW_GRID, mapModel.getPieceDataSize());
        assertEquals(COLUMN_GRID * COLUMN_PIECE * ROW_GRID * ROW_PIECE, mapModel.getDataSize());

        // test if all pieces are initialised with zeros
        for (int i = 0; i < mapModel.getCrc().length; i++) {
            assertEquals(getExpectedCRC32ValueForEmptyData(mapModel.getPieceDataSize()), mapModel.getCrc()[i]);
        }
    }

    @Test
    void testParseRealPacket() throws Exception {
        final String MAP_MODEL_PACKET = "<iq to='to' type='set' id='0DE5BFF098A0'><query xmlns='com:ctl'><ctl id='W3jcB-11' i='1675619197' w='100' h='100' r='8' c='8' p='50' m='1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,405855736,303969177,369753284,3855924242,1295764014,1295764014,1295764014,1295764014,815663441,2763375989,1263960677,1295764014,1295764014,1295764014,1295764014,1295764014,3480335780,85136059,3084431172,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014'/></query></iq>";
        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAP_MODEL_PACKET)).getMapModel();

        assertEquals(50, mapModel.getPixelWidth());
        assertEquals(100, mapModel.getColumnGrid());
        assertEquals(8, mapModel.getColumnPiece());
        assertEquals(100, mapModel.getRowGrid());
        assertEquals(8, mapModel.getRowPiece());
        assertEquals(64, mapModel.getCrc().length);
        assertEquals(10000, mapModel.getPieceDataSize());
        assertEquals(64 * 10000, mapModel.getDataSize());
    }

    @Test
    void testIdentifyChangedPieces() throws Exception {
        final String MAP_MODEL_PACKET = "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='0B62E77B90C0'><query xmlns='com:ctl'><ctl id='90542496' i='31304360' w='100' h='100' r='8' c='8' p='50' m='1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1418417320,3017914066,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1585359249,1511335613,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014'/></query></iq>";

        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAP_MODEL_PACKET)).getMapModel();

        System.out.println(mapModel);

        for (int i = 0; i < mapModel.getCrc().length; i++) {
            if (mapModel.hasData(i)) {
                System.out.println(i);
            }
        }
    }
}
