/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.info;

import de.caterdev.vaccumclean.deebot.smack.packets.info.UserInfosPacket;
import de.caterdev.vacuumclean.deebot.core.UserInfo;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.impl.JidCreate;

import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserInfoPacketTest extends AbstractPacketTest {
    public static final String to = "to";
    public static final String id = StanzaIdUtil.newStanzaId();
    public static final String jid1 = "jid1";
    public static final String rights1 = "userman,setting,clean";
    public static final String jid2 = "jid2";
    public static final String rights2 = "";
    public static final String jid3 = "jid3";
    public static final String rights3 = "userman,setting";

    public static final String PACKET = String.format("<iq xmlns='jabber:client' to='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl td='UserInfos'><u j='%s' a='%s'/><u j='%s' a='%s'/><u j='%s' a='%s'/></ctl></query></iq>", to, id, jid1, rights1, jid2, rights2, jid3, rights3);

    @Test
    void testParsePacket() throws Exception {
        UserInfosPacket packet = PacketParserUtils.parseStanza(PACKET);

        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(id, packet.getStanzaId());
        assertEquals(3, packet.getUserInfos().size());

        packet.getUserInfos().forEach(userInfo -> {
            String rights = userInfo.getUserRights().stream().map(Enum::toString).collect(Collectors.joining(","));
            switch (userInfo.getJid().asUnescapedString()) {
                case jid1:
                    assertEquals(rights1, rights);
                    break;
                case jid2:
                    assertEquals(rights2, rights);
                    break;
                case jid3:
                    assertEquals(rights3, rights);
                    break;
                default:
                    throw new RuntimeException("unassigned user " + userInfo.getJid());
            }
        });
    }


    @Test
    void testCreatePacket() throws Exception {
        UserInfo[] userInfos = new UserInfo[3];
        userInfos[0] = new UserInfo(JidCreate.from(jid1), rights1);
        userInfos[1] = new UserInfo(JidCreate.from(jid2), rights2);
        userInfos[2] = new UserInfo(JidCreate.from(jid3), rights3);

        UserInfosPacket packet = new UserInfosPacket(JidCreate.from(to), userInfos);
        packet.setStanzaId(id);

        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(id, packet.getStanzaId());
        assertEquals(3, packet.getUserInfos().size());
        packet.getUserInfos().forEach(userInfo -> {
            String rights = userInfo.getUserRights().stream().map(Enum::toString).collect(Collectors.joining(","));
            switch (userInfo.getJid().asUnescapedString()) {
                case jid1:
                    assertEquals(rights1, rights);
                    break;
                case jid2:
                    assertEquals(rights2, rights);
                    break;
                case jid3:
                    assertEquals(rights3, rights);
                    break;
                default:
                    throw new RuntimeException("unassigned user " + userInfo.getJid());
            }
        });

        assertEquals(PACKET, packet.toXML(null).toString());
    }

}