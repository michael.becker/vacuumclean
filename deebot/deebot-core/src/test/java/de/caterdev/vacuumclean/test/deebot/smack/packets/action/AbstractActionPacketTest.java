/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.Query;
import de.caterdev.vaccumclean.deebot.smack.packets.action.ActionPacket;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest2;
import org.jivesoftware.smack.packet.IQ;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractActionPacketTest<T extends ActionPacket> extends AbstractPacketTest2 {
    @Test
    void testCreatePacket() throws Exception {
        T packet = getInstance();
        packet.setStanzaId(id);
        packet.setInnerId(innerId);

        assertEquals(from, packet.getFrom().asUnescapedString());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(IQ.Type.set, packet.getType());

        assertEquals(getStanze(), packet.toXML(null).toString());
    }

    @Override
    protected void assertPacketsEqual(Query packet) {
        super.assertPacketsEqual(packet);
        assertEquals(from, packet.getFrom().asUnescapedString());
    }

    protected abstract T getInstance() throws Exception;
}