/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.core.map;

import de.caterdev.vaccumclean.deebot.smack.packets.info.MapPInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.info.TraceInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.GetMapModelResponsePacket;
import de.caterdev.vacuumclean.deebot.core.DataParseUtils;
import de.caterdev.vacuumclean.deebot.core.clean.CleanReplay;
import de.caterdev.vacuumclean.deebot.core.map.MapInfo;
import de.caterdev.vacuumclean.deebot.core.map.MapModel;
import de.caterdev.vacuumclean.deebot.core.map.TraceInfo;
import de.caterdev.vacuumclean.deebot.core.map.TracePoint;
import de.caterdev.vacuumclean.deebot.core.ui.CleanMapData;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.apache.commons.io.IOUtils;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ScaleMapDataTest extends AbstractPacketTest {
    @Test
    void testParseMapPInfoPackets() throws Exception {
        final String MAPM_PACKET = "<iq xmlns='jabber:client' to='user' from='bot' id='019F4D5D0610' type='set'><query xmlns='com:ctl'><ctl id='2VPR4-49' p='50' r='8' c='8' w='100' h='100' i='1675619197' m='1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,405855736,303969177,369753284,3855924242,1295764014,1295764014,1295764014,1295764014,537589243,1905558671,1263960677,1295764014,1295764014,1295764014,1295764014,1295764014,2910585383,3735169232,3084431172,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014'/></query></iq>";
        final String[] mapPMessages = {
                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='021D208F0770'><query xmlns='com:ctl'><ctl td='MapP' i='1023086233' pid='27' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kHoLTL8U6PAFLsX7Jhrz0KgA='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='0EA11CE34DE0'><query xmlns='com:ctl'><ctl td='MapP' i='1023086233' pid='28' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kHoLTL8U6PAFLsX7Jhrz0KgA='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='0E2EBEDE8010'><query xmlns='com:ctl'><ctl td='MapP' i='1023086233' pid='35' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kHoLTL8U6PAFLsX7Jhrz0KgA='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='0DEC179C50C0'><query xmlns='com:ctl'><ctl td='MapP' i='1023086233' pid='36' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kHoLTL8U6PAFLsX7Jhrz0KgA='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/20e87245' type='set' id='05DDE937E360'><query xmlns='com:ctl'><ctl td='MapP' i='1020122546' pid='27' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXIvssB1zarx5gMcusksWmIQP7Jn5FxkozQa+1vSHLtgSlOy4FfoJl6B1oY/Pt88WIoAvZt1FCoOU4u3fJ1rX/sJzbUC43sU/wq0S8KdPAeENb/s3J902Glr0Zl+mIFhtX9RlIeJHDqx9XHlyd0qY3vcgFyTx8XDuZ555MktjcN0BbiTi12Zi7Ilrcr6AqMofLuaKNjuPMpU5dh/szrOoTYs0ImLH7hJSsS5gVbzWV80zomlFkyd7O4NWJA2VOOr3UfZkCTJfF8l0vXc18cT8jygDviQlHwA'/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/20e87245' type='set' id='036E683570B0'><query xmlns='com:ctl'><ctl td='MapP' i='1020122546' pid='28' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5QTDVQUyfGfITQAdJ4FpPN/WerrK24VYuN3uMzPGoUUrFYZI5iKMYmI0UmaIABMFqjSLmN+hEkOUT3kRpDNkSxu07P4q0aRvDJIwqsJle7cWeja+mMRVb1D5DYK5oVEQ4mh+7KhyxK8D6ro5C/wDtI0bYy90MWscNQE2U+K9HOIREhUl2PYGr4STgfZ+MVzzkl1AWZIZ15hRiJb9FeSDKMkHmtjRt5AYjtzvF8T8AWhAq+F3/du2ZfEYxU24yrId3ozdh86pTWkgq+9plqdu1DaxfoF4r7bd13db+zEliIRPeT78Hxt8JR5KTEL9ipl3jjN43ZfNyY7eaWGuAwWP/66LZXjwYdmNexQIxLAmcPddaZK5+/yyElRESBsuhmpp+8oEyVoRflUu3j/YO9ORk1+nYLiKLwSkcYRxW84aQsBK2fkrSqip4Pn8tXFgzji0y7aBfCAXHvLG+a8IDhSCWpFKG9jz1JYNIkkd5uHp5yvwGD3yrMqIaLnApc5WWbrifXTBe0Vr2xZofynxqoM+R3MDHCd0BRpoicWSp7kRVnlPBCRxGqlqyg8B+5G3Sm6CLJFvx+IzC0vzKkylDX7r6TCxtPIqByqTXgvxptX/xQ9/wrYojtQBXkv1sq5GBLdM33EjoHaY/tzco48ujs8jhWOTG7q/bupOR1t9NXCqanqWo8W0CiAyvaXWz/utbvBfaL6PBkMiQyFhqvV2ZpoQB784QnVaOygk5dEXlCLwnqhWc5hoAH6m9oMp6TrfxUm7N5F+NwHKnc/J6bmjfuxRUc84gNrq8vA2vrwEBez66KqxUBnq7LlS6XSo='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/20e87245' type='set' id='024E42972F70'><query xmlns='com:ctl'><ctl td='MapP' i='1020122546' pid='36' p='XQAABAAQJwAAAADuCknH+OorF6YgzZbDsEipr1WZNk+k16WItIZ0lBOqSUDpcMFXBXbrnamE6qG30Mkr192JznsVo0i1yoljcRNJ7DCqpoWEf1W9jiK2MB6Nk5jaQbJlelkXOzlx4s//knyvkgHiBE2Tl7QFDucAd5RopqvhIe0k43yAGO/wep1tsf7xrA3apaiGeB0tjVMQRqEoUVT5N6Mf3/E9HF5AkV0v2f4bZe6T5E4J9QzJcj5lwAw2Ymcwgr/7JWsewCJTg58hm1ycc38eLO4Czk/F4iBICHpLFK30C+F4p6AVnn8pAA=='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/20e87245' type='set' id='0C1198604940'><query xmlns='com:ctl'><ctl td='MapP' i='1020122546' pid='27' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXIvssB1zarx5gMc9TK1Sl9VzWtkXYKULr9wrpl23GHB+2Y4xkdh4lx0fhBA4ln2XJ+r+Lo6U8y4Yjq5e0oGJfXnZaY35ld7SwGuthAKKUOovDl9Ubz9A3J/z7iHE3wc6IoR4I0a2Ot58qH4rZRga3bltmwg1cgxh2ilffQb+9qOFesMGgU55fm6fpgL50jT7l/i4ftDr2SafrNDZZQ5JgNRMXDFN+pQRfO9HcDE3qgf5B9sokQ+4belctjJ4Mo4c4wL4ZxLe08dbI1mty1IPs7ko1VQ9ugqlD09AA=='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/20e87245' type='set' id='0DB2CEFD5030'><query xmlns='com:ctl'><ctl td='MapP' i='1020122546' pid='36' p='XQAABAAQJwAAAADuCkqY7dLXq308yhSvFfZ8/KJA+ozOrXde6x+Ojg4/xqNxMMWSpGrI6tVxg+veA3Di8PPh+ia3XhMHMOt208kDqW4zKTKCbORBcrAYmfx1le0LBQnuws4/GzjBF3TmHqCdXptSWf2zaAFVVA5CboRr0AbnSUGdSNyp98xldT3/6gwPtaiRQEumUKBBuCUbi8ozyb+RIaug0rbZat4Bh+9iw/yMIaA5fYRlP9BnWu40QjOLcQA='/></query></iq>",

                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='0A25F68389D0'><query xmlns='com:ctl'><ctl td='MapP' i='1920629396' pid='27' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kCjqIcQ8kP43OenI5j4bmfFIeKdHYmdPBg6Q1d0G/wWJCNZwO5yg3CUNJey+uzDPhNtlIayrpUhLEF7vsHbmNEJ0SdDRlQf4YO5di/CqeZTpNR5MEKqoVEV+6VxdzdgX8zIRfyyyNWYDKuSuKZqp9AMpAEe9kAr2TYMy1rjgqIVb1PX4ITXCyQA+Zm9qzaZ5Qx0tCS5Duosd52JBg36aoLxb0Y/OGOmXcZRbgLVhQW5Dm392a5u5NYhQ5yHFymnHurGg2Y0Cy'/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='04E20E557480'><query xmlns='com:ctl'><ctl td='MapP' i='1920629396' pid='28' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kEMUGeIqSPw+qifAMZw6asg8c6Gbv/vrkDKinFNCzTE+QdeK2DcoyNV4M8loTGjp/3p80yprxFXyTZflTbb9OIwOKVE6y2bUbxTN+relut/oalIs+vL4xv9MYwzKVUrhdEjTX0TX/jZgObGoRpaqBmJREh27F5KU92/mtNRc6s1bnTPHVvXwKPDrgG9dY+wn4OdYKji1eIf/JT+HN3gvOyg3LDrZX+Yz8H/xtzJHwNqVkZZmHU2V1gNDW0tdvrmg='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='02EE019CF330'><query xmlns='com:ctl'><ctl td='MapP' i='1920629396' pid='35' p='XQAABAAQJwAAAABuIkn0FgIwJFcF2+RRL2+7vLNnJvyZYXZg75aTF6m2I6ReeZSqS61KSO7IWcPVivCBrmgMJ9b3u4zKdK3ixe/22BTG1y+GlJGweH8pHeXhZJCtdpgcC3DLjMhB2UskIfpeyNdkHWTlVwZDrMl/Spolz5DuRuQ2yw5DlGkUZ7zdj77AODG0ZO5T85LJXg=='/></query></iq>",
                "<iq to='dcjl2q7ac46b2641@ecouser.net/12df23b2' type='set' id='0D9E22728D80'><query xmlns='com:ctl'><ctl td='MapP' i='1920629396' pid='36' p='XQAABAAQJwAAAACAS2FxZtAt3d/dPhfIns9MNKhsEZ01eHRCjojuhKszLeHqWO0m1z+sFY2I9cgfMc+JmjWyM3KTrsX+Ir/cU2iwAA=='/></query></iq>"
        };

        //"<iq to='dcjl2q7ac46b2641@ecouser.net/20e87245' type='set' id='036E683570B0'><query xmlns='com:ctl'><ctl td='MapP' i='1020122546' pid='28' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5QTDVQUyfGfITQAdJ4FpPN/WerrK24VYuN3uMzPGoUUrFYZI5iKMYmI0UmaIABMFqjSLmN+hEkOUT3kRpDNkSxu07P4q0aRvDJIwqsJle7cWeja+mMRVb1D5DYK5oVEQ4mh+7KhyxK8D6ro5C/wDtI0bYy90MWscNQE2U+K9HOIREhUl2PYGr4STgfZ+MVzzkl1AWZIZ15hRiJb9FeSDKMkHmtjRt5AYjtzvF8T8AWhAq+F3/du2ZfEYxU24yrId3ozdh86pTWkgq+9plqdu1DaxfoF4r7bd13db+zEliIRPeT78Hxt8JR5KTEL9ipl3jjN43ZfNyY7eaWGuAwWP/66LZXjwYdmNexQIxLAmcPddaZK5+/yyElRESBsuhmpp+8oEyVoRflUu3j/YO9ORk1+nYLiKLwSkcYRxW84aQsBK2fkrSqip4Pn8tXFgzji0y7aBfCAXHvLG+a8IDhSCWpFKG9jz1JYNIkkd5uHp5yvwGD3yrMqIaLnApc5WWbrifXTBe0Vr2xZofynxqoM+R3MDHCd0BRpoicWSp7kRVnlPBCRxGqlqyg8B+5G3Sm6CLJFvx+IzC0vzKkylDX7r6TCxtPIqByqTXgvxptX/xQ9/wrYojtQBXkv1sq5GBLdM33EjoHaY/tzco48ujs8jhWOTG7q/bupOR1t9NXCqanqWo8W0CiAyvaXWz/utbvBfaL6PBkMiQyFhqvV2ZpoQB784QnVaOygk5dEXlCLwnqhWc5hoAH6m9oMp6TrfxUm7N5F+NwHKnc/J6bmjfuxRUc84gNrq8vA2vrwEBez66KqxUBnq7LlS6XSo='/></query></iq>";

        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAPM_PACKET)).getMapModel();
        MapInfo mapInfo = new MapInfo(mapModel);

        for (String mappMessage : mapPMessages) {
            MapPInfoPacket packet = PacketParserUtils.parseStanza(mappMessage);
            mapInfo.updateMapBuffer(packet.getMapPiece());
        }

        CleanMapData cleanMapData = new CleanMapData();
        cleanMapData.setMapInfo(mapInfo);
        cleanMapData.writeHTML("target/mappmessages.html", 800, 800, false, false);
    }

    @Test
    void testScaleMapData() throws Exception {
        final String MAPM_PACKET = "<iq xmlns='jabber:client' to='user' from='bot' id='019F4D5D0610' type='set'><query xmlns='com:ctl'><ctl id='2VPR4-49' p='50' r='8' c='8' w='100' h='100' i='1675619197' m='1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,405855736,303969177,369753284,3855924242,1295764014,1295764014,1295764014,1295764014,537589243,1905558671,1263960677,1295764014,1295764014,1295764014,1295764014,1295764014,2910585383,3735169232,3084431172,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014'/></query></iq>";
        final String[] tracePackages = IOUtils.toString(getClass().getResourceAsStream("/tracemessages.iq"), "UTF-8").split(", ");
        final String[] mappMessages = IOUtils.toString(getClass().getResourceAsStream("/mappmessages.iq"), "UTF-8").split(", ");

        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAPM_PACKET)).getMapModel();

        MapInfo mapInfo = new MapInfo(mapModel);
        for (String mappMessage : mappMessages) {
            MapPInfoPacket packet = PacketParserUtils.parseStanza(mappMessage);
            mapInfo.updateMapBuffer(packet.getMapPiece());
        }

        mapInfo.writeMapData("target/map.info");
        MapInfo.readMapData(getClass().getResource("/deebot.map").getFile(), mapModel).writeUnscaledHTML("target/loadedmap.html");

        CleanMapData cleanMapData = new CleanMapData();
        cleanMapData.setMapInfo(mapInfo);
        cleanMapData.updateScale(800, 800);
        List<List<CleanMapData.Rect>> rects = cleanMapData.update();

        TraceInfo traceInfo = new TraceInfo();

        System.out.print("var positions=[");
        for (String tracePackage : tracePackages) {
            TraceInfoPacket packet = PacketParserUtils.parseStanza(tracePackage);
            int traceId = packet.getTraceId();
            int tf = packet.getTf();
            int tt = packet.getTt();
            List<TracePoint> tracePoints = DataParseUtils.parseTracePoints(packet.getTr());
            for (TracePoint tracePoint : tracePoints) {
                float x = tracePoint.getPositionX();
                float y = tracePoint.getPositionY();
                //System.out.println("(" + x + "," + y + ")" + " --- (" + cleanMapData.getPhoneX(x) + ", " + cleanMapData.getPhoneY(y) + ")");
                System.out.print("[" + cleanMapData.getPhoneX(x + 2000) + ", " + cleanMapData.getPhoneY(y + 2000) + "],");
            }
            traceInfo.updateTrace(traceId, tf, tt, tracePoints);
        }
        System.out.println("]");


        System.out.println(traceInfo);
        System.out.println(mapInfo);

        CleanReplay cleanReplay = new CleanReplay(cleanMapData, mapInfo, traceInfo);
        cleanReplay.writeUnscaledHTML("target/cleaningtrace.html");


        for (int i = 0; i < rects.size(); i++) {
            System.out.println(i + ": " + rects.get(i).size());
        }

        /*
        System.out.println("mainContext.fillStyle = '#AAAAAA';");
        for (CleanMapData.Rect rect : rects.get(0)) {
            System.out.println("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);");
        }
        System.out.println("mainContext.fillStyle = '#000000';");
        for (CleanMapData.Rect rect : rects.get(1)) {
            System.out.println("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);");
        }
        System.out.println("mainContext.fillStyle = '#DDDDDD';");
        for (CleanMapData.Rect rect : rects.get(2)) {
            System.out.println("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);");
        }

         */
    }
}
