/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.info;

import de.caterdev.vaccumclean.deebot.smack.packets.info.PosInfoPacket;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

public class PosInfoPacketTest extends AbstractPacketTest {
    @Test
    void testParsePosInf() throws Exception {
        PosInfoPacket pos0 = PacketParserUtils.parseStanza("<iq to='user' type='set' id='08C6D17A9180'><query xmlns='com:ctl'><ctl td='Pos' t='p' p='0,0' a='177' valid='1'/></query></iq>");
        PosInfoPacket pos1 = PacketParserUtils.parseStanza("<iq to='user' type='set' id='0F66639B4550'><query xmlns='com:ctl'><ctl td='Pos' t='p' p='108,-18' a='172' valid='1'/></query></iq>");
        PosInfoPacket pos2 = PacketParserUtils.parseStanza("<iq to='user' type='set' id='078574FEDBB0'><query xmlns='com:ctl'><ctl td='Pos' t='p' p='107,-18' a='91' valid='1'/></query></iq>");
        PosInfoPacket pos3 = PacketParserUtils.parseStanza("<iq to='user' type='set' id='04E0D2F137C0'><query xmlns='com:ctl'><ctl td='Pos' t='p' p='106,-18' a='13' valid='1'/></query></iq>");
        PosInfoPacket pos4 = PacketParserUtils.parseStanza("<iq to='user' type='set' id='0F9C845DDB10'><query xmlns='com:ctl'><ctl td='Pos' t='p' p='265,-11' a='-1' valid='1'/></query></iq>");
        PosInfoPacket pos5 = PacketParserUtils.parseStanza("<iq to='user' type='set' id='0AA3196881C0'><query xmlns='com:ctl'><ctl td='Pos' t='p' p='589,-19' a='0' valid='1'/></query></iq>");
        PosInfoPacket pos6 = PacketParserUtils.parseStanza("<iq to='user' type='set' id='0658C1A072C0'><query xmlns='com:ctl'><ctl td='Pos' t='p' p='890,-18' a='0' valid='1'/></query></iq>");

        System.out.println(pos0.getPos());
        System.out.println(pos1.getPos());
        System.out.println(pos2.getPos());
        System.out.println(pos3.getPos());
        System.out.println(pos4.getPos());
        System.out.println(pos5.getPos());
        System.out.println(pos6.getPos());
    }
}
