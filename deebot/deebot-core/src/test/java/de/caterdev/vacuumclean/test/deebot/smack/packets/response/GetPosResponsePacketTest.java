/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.response;

import de.caterdev.vaccumclean.deebot.smack.packets.response.GetPosResponsePacket;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

public class GetPosResponsePacketTest extends AbstractPacketTest {
    public static final String PACKET_BOT_POS = "<iq to='to' type='set' id='071F2A592690'><query xmlns='com:ctl'><ctl id='iAG7E-273' ret='ok' t='p' p='144,-684' a='-101' valid='1'/></query></iq>";
    public static final String PACKET_CHG_POS = "<iq to='to' type='set' id='0E0998EC32C0'><query xmlns='com:ctl'><ctl id='iAG7E-281' ret='ok' p='144,-686' a='101'/></query></iq>";
    public static final String PACKET_TEST = "<iq to='username23@ecouser.net/resource' type='set' id='0E4FD0F60240'><query xmlns='com:ctl'><ctl id='gZVhi-11' ret='ok' p='144,-686' a='101'/></query></iq>";

    @Test
    void testParsePacket() throws Exception {
        GetPosResponsePacket packet = PacketParserUtils.parseStanza(PACKET_CHG_POS);
        System.out.println(packet.toXML(null).toString());
        System.out.println(PacketParserUtils.parseStanza(PACKET_BOT_POS).toXML(null).toString());

    }
}
