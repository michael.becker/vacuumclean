/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.response;

import de.caterdev.vaccumclean.deebot.smack.packets.action.map.PullMPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.clean.GetCleanLogsResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapSetResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.PullMResponsePacket;
import de.caterdev.vacuumclean.deebot.core.map.MapDetail;
import de.caterdev.vacuumclean.deebot.core.map.Position;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

public class SimpleResponseTests extends AbstractPacketTest {
    @Test
    void testParseGetCleanLogsResponsePacket() throws Exception {
        String stanza = "<iq xmlns='jabber:client' to='username23@ecouser.net/resource' id='0D101C9E5AF0' type='set'><query xmlns='com:ctl'><ctl id='xzqFr-17' ret='ok'><CleanSt a='2' s='1564092832' l='43' t='a' f='a'/><CleanSt a='58' s='1563472545' l='3636' t='b' f='s'/></ctl></query></iq>";
        System.out.println(stanza);
        GetCleanLogsResponsePacket packet = PacketParserUtils.parseStanza(stanza);
        System.out.println(packet.toXML(null).toString());
    }

    @Test
    void testParsePullMResponse() throws Exception {
        String mapSetResultStanza = "<iq xmlns='jabber:client' to='username23@ecouser.net/resource' from='e0001057017609483161@115.ecorobot.net/atom' id='0F35BE4EC7B0' type='set'><query xmlns='com:ctl'><ctl id='pF2te-99' ret='ok' msid='7' tp='sa'><m mid='0' p='1'/><m mid='1' p='1'/><m mid='2' p='1'/><m mid='3' p='1'/><m mid='4' p='1'/><m mid='5' p='1'/></ctl></query></iq>";
        GetMapSetResponsePacket mapSetResponsePacket = PacketParserUtils.parseStanza(mapSetResultStanza);
        MapDetail mapDetail = mapSetResponsePacket.getMapDetails().get(0);

        System.out.println(mapSetResponsePacket.getMapDetails());

        PullMPacket pullMPacket = PacketParserUtils.parseStanza("<iq id='8353' to='$to' from='$from' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='PullM' tp='sa' msid='7' mid='0' seq='0'/></query></iq>");
        PullMResponsePacket pullMResponsePacket = PacketParserUtils.parseStanza("<iq to='username23@ecouser.net/resource' type='set' id='096592E68AC0'><query xmlns='com:ctl'><ctl id='2FR81-11' ret='ok' m='-8000,3650;-8000,4000;-7850,4250;-7850,5000;-7750,5300;-7700,5400;-7250,5600;-7050,5300;-7100,5150;-6950,4900;-6400,4700;-5350,4700;-5200,4850;-5200,5050;-5150,5050;-4650,4800;-4650,4650;-4450,4450;-4200,4350;-3750,4300;-3600,4450;-3500,4450;-2800,4150;-2750,3800;-2650,3700;-2750,3100;-2850,3050;-3300,3100;-3350,3300;-4150,3300;-4250,3200;-4900,3200;-4900,3300;-5150,3450;-6050,3750;-7400,3750;-7600,3500;-7950,3550;-8000,3650'/></query></iq>");
        System.out.println(pullMPacket.toXML(null).toString());
        System.out.println(pullMResponsePacket.getPositions());
        mapDetail.setPositions(pullMResponsePacket.getPositions());
        System.out.println(mapDetail.getPositions());
    }

    @Test
    void testDrawPullMRespult() throws Exception {
        GetMapSetResponsePacket mapSetResponsePacket = PacketParserUtils.parseStanza("<iq xmlns='jabber:client' to='username23@ecouser.net/resource' from='e0001057017609483161@115.ecorobot.net/atom' id='0F35BE4EC7B0' type='set'><query xmlns='com:ctl'><ctl id='pF2te-99' ret='ok' msid='7' tp='sa'><m mid='0' p='1'/><m mid='1' p='1'/><m mid='2' p='1'/><m mid='3' p='1'/><m mid='4' p='1'/><m mid='5' p='1'/></ctl></query></iq>");

        String pullM0 = "<iq to='username23@ecouser.net/resource' type='set' id='0C00E5753390'><query xmlns='com:ctl'><ctl id='lDAhL-11' ret='ok' m='-3300,-550;-3300,-350;-3150,-100;-3150,300;-2950,450;-3050,550;-3000,1100;-2800,1250;-2950,1400;-2600,2250;-2650,3350;-2550,3700;-1550,3650;-1450,3550;-1550,3200;-1300,2850;-1100,3150;-1250,3300;-1250,3650;-1050,3850;-400,3600;450,3600;850,3400;850,3100;650,2800;750,2700;1100,2700;1100,-1050;850,-1050;400,-850;100,-900;-100,-750;-700,-750;-1050,-550;-1200,-550;-1450,-800;-1450,-950;-2500,-550;-3300,-550'/></query></iq>";
        String pullM1 = "<iq to='username23@ecouser.net/resource' type='set' id='0B8F2285BF30'><query xmlns='com:ctl'><ctl id='lDAhL-15' ret='ok' m='-8000,3650;-8000,4000;-7850,4250;-7850,5000;-7750,5300;-7700,5400;-7250,5600;-7050,5300;-7100,5150;-6950,4900;-6400,4700;-5350,4700;-5200,4850;-5200,5050;-5150,5050;-4650,4800;-4650,4650;-4450,4450;-4200,4350;-3750,4300;-3600,4450;-3500,4450;-2800,4150;-2750,3800;-2650,3700;-2750,3100;-2850,3050;-3300,3100;-3350,3300;-4150,3300;-4250,3200;-4900,3200;-4900,3300;-5150,3450;-6050,3750;-7400,3750;-7600,3500;-7950,3550;-8000,3650'/></query></iq>";
        String pullM2 = "<iq to='username23@ecouser.net/resource' type='set' id='0422A8248190'><query xmlns='com:ctl'><ctl id='lDAhL-19' ret='ok' m='-8950,7050;-8950,7350;-8800,7550;-8850,8150;-8550,8900;-8550,10050;-8350,10100;-8000,9950;-7150,9950;-6950,9900;-6800,9750;-6600,9850;-6450,9600;-6650,9650;-7000,9300;-7000,8700;-6800,8600;-6950,8600;-7150,8350;-7050,8150;-7100,7900;-7000,7800;-6850,7900;-6650,7850;-6700,7700;-6750,7750;-7000,7600;-7000,7500;-6700,7200;-6800,7050;-6800,6400;-6850,6350;-7100,6400;-7200,6200;-7400,6100;-7400,5950;-7250,5700;-7600,5500;-8000,5600;-8300,5450;-8500,5450;-8550,5800;-8850,6100;-8700,6450;-8700,6950;-8850,7100;-8900,7050;-8950,7050'/></query></iq>";
        String pullM3 = "<iq to='username23@ecouser.net/resource' type='set' id='0745CE236C70'><query xmlns='com:ctl'><ctl id='lDAhL-23' ret='ok' m='-6900,50;-6800,200;-6850,250;-6800,500;-6600,500;-6400,250;-6300,500;-6200,550;-5850,500;-5450,600;-5250,1150;-5300,2300;-5050,2650;-5050,3000;-4950,3100;-4300,3100;-4150,2850;-3900,2750;-3650,2750;-3850,2200;-3850,1100;-3600,950;-3600,600;-3850,250;-3850,-150;-3700,-300;-3500,-350;-3650,-550;-4750,-150;-6350,-150;-6450,250;-6650,50;-6900,50'/></query></iq>";
        String pullM4 = "<iq to='username23@ecouser.net/resource' type='set' id='086789DCF820'><query xmlns='com:ctl'><ctl id='lDAhL-27' ret='ok' m='-6200,7600;-6050,8000;-6100,8450;-5900,8900;-5850,9700;-5500,9700;-4500,9300;-3200,9300;-3150,9250;-3400,8500;-3400,7550;-3550,7400;-3850,7400;-4050,7200;-4050,6550;-3950,6450;-4100,6300;-4000,6200;-3800,6250;-3700,6150;-3700,6000;-3750,5500;-3850,5400;-4050,5500;-4250,5400;-4200,5000;-4500,5050;-4600,4900;-4700,4900;-5400,5250;-5500,5550;-5700,5650;-6000,5650;-6100,5750;-6100,6200;-6000,6300;-6050,6450;-5950,6600;-5950,6900;-5800,7100;-5800,7450;-5950,7550;-6200,7600'/></query></iq>";
        String pullM5 = "<iq to='username23@ecouser.net/resource' type='set' id='03969782CAD0'><query xmlns='com:ctl'><ctl id='lDAhL-31' ret='ok' m='-3550,4900;-3250,5050;-3150,5250;-3150,5650;-2950,5850;-2850,6100;-2850,6700;-3000,6800;-3000,7150;-2900,7450;-2250,7200;-1200,7200;-1100,7300;-1300,7450;-1250,7750;-950,7800;-800,7950;-750,8400;-600,8550;-650,8600;-600,8850;250,8500;1100,8500;1100,5350;750,5350;550,5100;600,4250;350,4200;250,4350;50,4450;-450,4450;-550,4550;-500,5150;-850,5400;-1350,5550;-2050,5550;-2200,5450;-2300,5350;-2300,4900;-2500,4700;-2750,4750;-2950,4350;-3450,4500;-3450,4800;-3550,4900'/></query></iq>";
        String[] pullMs = {pullM0, pullM1, pullM2, pullM3, pullM4, pullM5};
        String[] fillStyles = {"#FF0000", "#AAAAAA", "#BBBBBB", "#CCCCCC", "#DDDDDD", "#EEEEEE"};

        for (MapDetail mapDetail : mapSetResponsePacket.getMapDetails()) {
            PullMResponsePacket pullMResponsePacket = PacketParserUtils.parseStanza(pullMs[mapDetail.getMid()]);
            mapDetail.setPositions(pullMResponsePacket.getPositions());

            System.out.println("mainContext.fillStyle = '" + fillStyles[mapDetail.getMid()] + "';");
            System.out.println("mainContext.beginPath();");
            System.out.println("mainContext.moveTo(" + mapDetail.getPositions().get(0).getX() + ", " + mapDetail.getPositions().get(0).getY() + ")");
            for (Position position : mapDetail.getPositions()) {
                System.out.println("mainContext.lineTo(" + position.getX() + "-xModifier, " + position.getY() + "-yModifier);");
            }
            System.out.println("mainContext.closePath();");
            System.out.println("mainContext.fill();");
        }
    }

    @Test
    void testParseMapStInfo() throws Exception {
        String stanza = "<iq xmlns='jabber:client' to='$to' id='057275A97EE0' type='set'><query xmlns='com:ctl'><ctl ts='862290595' td='MapSt' st='relocGoChgStart' method='' info=''/></query></iq>";
        Stanza packet = PacketParserUtils.parseStanza(stanza);
        System.out.println(stanza);
        System.out.println(packet.toXML(null).toString());
    }
}
