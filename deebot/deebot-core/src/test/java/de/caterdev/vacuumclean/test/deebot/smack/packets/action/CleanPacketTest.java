/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.action.CleanPacket;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.impl.JidCreate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class CleanPacketTest extends AbstractPacketTest {
    public static final String id = StanzaIdUtil.newStanzaId();
    public static final String to = "to";
    public static final String from = "from";
    public static final String ctlId = StanzaIdUtil.newStanzaId();
    public static final String cleanType = "auto";
    public static final String speed = "standard";
    public static final String act = "s";

    public static final String PACKET = String.format("<iq xmlns='jabber:client' to='%s' from='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl id='%s' td='Clean'><clean type='%s' speed='%s' act='%s'/></ctl></query></iq>", to, from, id, ctlId, cleanType, speed, act);

    @Test
    void testParsePacket() throws Exception {
        CleanPacket packet = PacketParserUtils.parseStanza(PACKET);

        assertNotNull(packet);
        assertEquals(id, packet.getStanzaId());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(from, packet.getFrom().asUnescapedString());
        assertEquals(ctlId, packet.getInnerId());
        assertEquals(cleanType, packet.getCleanType());
        assertEquals(speed, packet.getSpeed());
        assertEquals(act, packet.getAct());

    }

    @Test
    void testCreatePackage() throws Exception {
        CleanPacket packet = new CleanPacket(JidCreate.from(from), JidCreate.from(to), cleanType, speed, act, null);
        packet.setStanzaId(id);
        packet.setInnerId(ctlId);

        assertEquals(from, packet.getFrom().asUnescapedString());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(cleanType, packet.getCleanType());
        assertEquals(speed, packet.getSpeed());
        assertEquals(act, packet.getAct());
        assertEquals(IQ.Type.set, packet.getType());

        assertEquals(PACKET, packet.toXML(null).toString());
    }
}
