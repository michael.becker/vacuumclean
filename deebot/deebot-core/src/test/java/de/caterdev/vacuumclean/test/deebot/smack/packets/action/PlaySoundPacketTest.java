/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.action.PlaySoundPacket;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.impl.JidCreate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class PlaySoundPacketTest extends AbstractPacketTest {
    public static final String id = StanzaIdUtil.newStanzaId();
    public static final String to = "receiver";
    public static final String from = "sender";
    public static final String ctlId = StanzaIdUtil.newStanzaId();
    public static final int sid = 1;

    private static final String PACKET = String.format("<iq xmlns='jabber:client' to='%s' from='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl id='%s' td='PlaySound' sid='%s'/></query></iq>", to, from, id, ctlId, sid);

    @Test
    void testParsePacket() throws Exception {
        PlaySoundPacket playSoundPacket = PacketParserUtils.parseStanza(PACKET);

        assertNotNull(playSoundPacket);
        assertEquals(id, playSoundPacket.getStanzaId());
        assertEquals(to, playSoundPacket.getTo().asUnescapedString());
        assertEquals(from, playSoundPacket.getFrom().asUnescapedString());
        assertEquals(ctlId, playSoundPacket.getInnerId());
        assertEquals(sid, playSoundPacket.getSoundId());
    }

    @Test
    void testCreatePacket() throws Exception {
        PlaySoundPacket packet = new PlaySoundPacket(JidCreate.from(from), JidCreate.from(to), sid);
        packet.setStanzaId(id);
        packet.setInnerId(ctlId);

        assertEquals(from, packet.getFrom().asUnescapedString());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(IQ.Type.set, packet.getType());
        assertEquals(sid, packet.getSoundId());

        assertEquals(PACKET, packet.toXML(null).toString());

    }
}
