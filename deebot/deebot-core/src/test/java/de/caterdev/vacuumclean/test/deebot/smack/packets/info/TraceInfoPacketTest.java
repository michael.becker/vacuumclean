/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.info;

import de.caterdev.vaccumclean.deebot.smack.packets.info.TraceInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.GetTraceInfoResponsePacket;
import de.caterdev.vacuumclean.deebot.core.DataParseUtils;
import de.caterdev.vacuumclean.deebot.core.map.TraceInfo;
import de.caterdev.vacuumclean.deebot.core.map.TracePoint;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.apache.commons.io.IOUtils;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TraceInfoPacketTest extends AbstractPacketTest {
    @Test
    void testParseSingleTrace() throws Exception {
        String[] tracePackages = IOUtils.toString(getClass().getResourceAsStream("/tracemessages.iq"), "UTF-8").split(", ");

        TraceInfoPacket packet = PacketParserUtils.parseStanza(tracePackages[37]);

    }

    @Test
    void testParseAllTracesInOne() throws Exception {
        String actionPacket = "<iq to='bot' from='username23@ecouser.net/resource' id='3tKBN-11' type='set'><query xmlns='com:ctl'><ctl id='3tKBN-10' td='GetTr' tt='1540' tf='0' trId='967551'/></query></iq>";
        String responsePacket = "<iq xmlns='jabber:client' to='username23@ecouser.net/resource' from='bot' id='0907E007D270' type='set'><query xmlns='com:ctl'><ctl id='3tKBN-10' ret='ok' tr='XQAABAAZHgAAABUAGj5wgY5xThs9NhcWYX8tyVcw3z73ovznRqR6xydrTWEK4QRZS216WBJDSzED4NRiT/VzQ6+UKXspPnN6k94JEPLFAzRDTLJtXhmAaP8i163b+FGHX1/fPGp+HmnRjF7qQRGbDCgV1dcA+JHph1m4P/aU/yi8VS6v/NuitNzd0xMe84tNqnCkbOi4GherO8+64gB2TNRuVL1NOdua/9lkH4Tw/DxJUQkPdTM+cCKgzAMUs64w1IfbFrKvL7gvQpbOFFvKgqXmWQtlqfuIYDhbR6k42FfDiQJVNq4TMcn6OnoVBWB1dZEUT2lEXhxTSOw9kD4EJrRWdlKEZ/fWl7IEn7ySB3LKffVWNWokZtS7PkSiAhrvz6h3hxw9EWNfg8VdLVzOleVyUb9h60Z3ttf91epfYkr7ccOWfA4Td3hNIjzfoD7h+HgMXzChT/FXaL5iC0NZyHgZLG19+gUyPvCh2b7ALDNgU96HpblSePmcThq44tQhRFHavHdUGOL0YDDAz6HVU/fCrxnfY6GC9Hpp0QaIWZGrHpot3Wmcl0X1dI/I6sW1VKrhFL42gatwePDIsUlWrXhDYAZM+3WbGqtCPAZjcCzW9P5+WD0Mmc9Ub5gRdqQIwh78+TWsexSH4RKLpEnD9hXcwuGJCNBIxGNUed3KYmu+uuFNJD7yWQPDSOqTeWjJv10ViPAKwjAMEjEl4w79zUzpPAFGg61/acNUR4ZmheCwCosqSfsACupKRzkvV4UpuISJb65yRkmEUaXUWD0gq5nWZaQM1dCkzy+cpUq4m7TQWX99w13iOTdma8i/5MtljMjmYu4r/IItX9IdCEwcWK8naZrh63j/kR3f1qTPf9CdqIz4U8PYf/uFKbo1kdlalWvP14eeuVqcJKHX5xqmWJA2xs34569zlg67RaCKyLyhhUa1iu7CvkoMkhHfrXQUO1XahQGoIQeR8YKT8x1iwwuIjetbvdBJAOvDGnlHUJzPXkTEjF3DTpwCgmVO8oPcMqUYgGjkwODkYiuqwq9d309qRz9Fs0bDInt1/82zXJa6rRyfL8XG2cXbMKKDF/insKPzfqyRJ1C3O5RreZnqkE863yU1nze6oFKv2ugO3tnxzuuPYddKmHrTYtB+G9+IHhO6DClXqQGbdtlE5Ad5BWFzNnAFgSovJRmJVLw5Z7fM8X5cLreg8XgVCXQPwwYwYX69c8FiV+VEZq0U5D1xWjCkPXmc+Em0SoTq0dABLYb3968IPhEFP08ijdyouXAPsMcr9bA/5g1CDMwI4csnUTkfQxmkrfDDJIjYUdKxDkuwceAh6/Dp/VI8jT8GmpytEs+rTlGCPzObwyTaxIlzG4g6y41Ebc0dtdp/Z4TyvkNp+RVfBTFGLqD0VAg0sXTx8Y3I8ExW6SQ4AvrVyiSVotHMGU1wkOEoimiCP8lpezft4g9Knhsjgev9Gx1Gh0XYbuTovdc2sCmUI2/AiOwV5apWMloKKV6dzRh0EBfJfOusGZ7pvx8Qtnn8gOPFnyKLVrd5HCEKhtbwN0k4fR/5ltG9IvtTIFw6drjPZHi3hEH8nZS3LJ0ZkeLh/gHbQBpLCLvpbD08TNri1z5RmyLuEo+klMhsnV5tfnn7HuXoLn88uKNoOGyIT3JMVXBZkOY1TrkC5yMbq4wW1gebFsx1yiPLNAAoa1hAalAmYLIF5S4kr9ypNMdcuVed/jXL5Thd/2GV/zG9QuOHfo8lrRcGbyBfxD88IkYBv+I1R08fQybpkS0wIqoKN0hcWZLQAYVrTDxGrJQGHzntydAjMab5nWesHf9x6ODZBCm7LK+cqZXIkWd6YIJb9lPWGxnY2gbAH2N7Uy2CfKlxZcmGL+qIBLvce3Kqgourxp0lmyaRw19kNhT5jXczWqTNno1wL4KEBPALFjOGmjFgFWb1STMnG2nTMck8J1gdoHS6fwK9Am9EbgDHJGM0zjUYsKSAUtIS663PYvz3+3fAcP7VNzSB0MKUjFC2fyp8v++irzo8sBEP+/zE6V39dtU43gO+DdijRicreKERHqAPHEIPBkQxiMdRD2zM4UkjU0PcNWl3jZ8dvPyOWRoM3EtmPnlCP6z/zP/rAKuY9/AXS4EkX5Z7uUGx/d7TgnMsbu6SdLo/H14xzs8HN+pcm4w2LvHU4pRRG1h3ftL1PioAZ83tyFs1xZCo8NOGUDtOYrZgHybHyyALUC7KJ07MQFjYb5KXE9Q1PFNn28O4h2wHWGy+sSlP4ByzuEu6zMWBAKj5t4mLmS7t6JgIxuoopYOenFWatoqutVhvdQRNLRgrn6WEDFKfLkd3x+YilLAatrpuAI16DeKZj3Re8ipNDdR618OMQNkDfPmBrH/GUVYt6rHCZ2aLs61Oi28J5viev4zywgedI5wH4xxksIboJCxSwQ26yCmBgeuywGC+TeUVjT6otbhyecrFNaLDJU0g1tDh6DsH4xB8PbY3OonQYZFK0pXqQtMSXDEobeudAOFShwZF9Dx+UyNW1JOF1uY8EHQyMkoRfP0HlT7mLBTGYf7N4nhS5F7FnHRxhfy3j0U+L5DL9wAWLStEQWCXBplhSeQgErDe3mol3Iu0Klng/t42uwLR1uBWgxotA2cFa9fflO0YPkFiMPX4uEcz/7T+z9pejQC7fEUKsvUpKSRqGzpUcAdfx7HAQby7wFooOMJ1pz0dRLlq9MiIIouAOWqw3/GvxcYwR1O1Uw+DCX7duFkUJt6Avzwnib4WYrQF4PsB9vZBMk55W2DeoLgRmbvBrhoiE0neGcgS5tOkJvZFO8lTWY5d2LBuYzCHxWwyflyHZ2V+n4VDzgcSxq3ePZnmUaL+bxtRPFGg4WW3VoR3M3f0QrQnRPcJIqOkCPLUN759VqWaqQ8JV8ZfyimVEZR9RpxR447RlEljmzDEvLI57Q9FS8+uVvX8HGPO4YZQ1EVlzXYsiD4Z+ItJtFcfq2kS3xq6+hQHTJC4/eHp9BIRvkWksGhgjyR9D5YioO4KvcB4Mopr/T4oNaDGLUWEPMSVgKqpAccboPWXG32ZOoGa0HIN4FHTIj69PZCKI9+UYBLrZYy1EDX4o9zREWUWeIu0aeyZ47hJvfA1gQgGgn6eHMp+d8+2wl+KWv4wmX3C4+1C5oBjigN2O4MMiffz7t1BJIOshEVX4lUhk4yy7bhISSCFaQc5yU5hIiKkV8FSDVQoPmYtlqq47qTbtofeLXr5RXviQbdXpXpgE2vQxc3KXts459FbuxdFsAp8lbXXHoU+rHYP4oG6zVr6tD6iCTF7hualpJuHNkgS8fXWP63uOMRzP18SGU85cwAIiiNRu2+4T5MEwNO7M+2sVymAcl2yB7IfcljIcz4uXOzFy5ZRnV5zEXN70y8lqhAu43hMjecdW0qGMzDDC3V8EYRbjihRViRZW/4KlOTkpR4N244zIuEATr8Pdc5M8BghHqbb30VSI9ex4yxqgGWJbY61rJ5HhGxJ2HkY0BCXFOYZx4STwFma5KIwOSgD6FRyKm33ULMi9gc0VDQeLwcC3Snisau5nhz5mCtE9/mk6NhK5ob7GH7pDFCkr3TpKuplyTwAIRwn+z+ZnukyPYcPnWG/a8+zrha7w4bjDC3PIYh+glEpenyyc4Qj11Ka8rEI0UH2S9DLE04OtPNUFfYYq5BH4bXq+oHifCEbAJ52gC2rsG7jcRjjPN0a3cgZk5no9DzJGZpnHRbCj76Hozh72qLiIWoOQy58D6Fp4xhC34wvUWqZ311MmE8LUtPWawmnXtJ6JqGZjJLwheFQlsPp4FV5iVnocyPLM4AQc3AlYjtOUDzyvlb+tGFq8yboBf1BKcdOJQzDocHZ6/mBVOoR2J0mLesNBWwt4e6fglosQ9Xb3ZUk2mcfHZr85dvnl1qol58YieXu20xce3t7rWcn4LU8piECDnUNw6DEsI+m2Imqvu0c+wWo75iK1OehQfBXfGR4/xeEXeFNmvQhhxo8ZduzOFeOwEhfzr3ExqSbDr+xENuSDgZuER5p1YaLJx3sbJ60fOq/xj+89mux0hKBtE23e1w2V725vGe20alrcCkwO500Qz/d8XIvIjV05fNREN3fjtaEp+ie8gWzJCejQxjcIdE1hGrzxCYKP6PTh7luhhWgUJH1j/rPmKQ81oDtj+i2+z1AtvCAIwDjJY+h7Sq6YZbxrSrepvLS1eHVTvjtiaKR6dMPYHz9y5Gjayp3Z4ZRB88VQHV4lZKCi6xTj13Wn9QlN2xhXo3pWUK5ekUyX+e/8L+ffneVVVtYoPvBlfWRgVwG1SDBOFl84xqN8lOZ6ikT61LTaWLw8INmwv9RlD+KvE0GWCdrZUUFUgKJoygcBxkOVBZeVPi14rEytrG+qmvOUuXPcWTmdd/GspuoYTc95sDhhXggfrSN7AN9eR7oP1LkTOb2vmtWAounDI+e5WqHQkmwP/RyrVsnZ+FG4lEtCpDcG4TcR32LRJ4ebQ5NQc+CyxT4isu6WPsUWtaVKS7fJbE8qsGmwM5EaxoiYnyx0ZZctSBD8VAcM6dVgKdzJFG73Gh4HXNBd82DRvcpiT7GVvyg7XtxqsVPMve5d4QmXLMtT5crwmyXgXcZpZj/O2bAmYi1Ozb4duVlvUE4O+b5OHbGuUtqeN7wyKFTwpR95naLnirt4xk76y8HC7SjQsPPL+5O1IFRn8fSVc0iHPzBmDHok4x9jRAGUbeUmJn3hRhSIx4uWAFneyYEQWk4FtrfPFYnGhBLyiK/N2bVkEPnHuqg02KrJeARI8SWGfyZ+xkKGJcTD4/cUHRZpGYHXn8wNBMF11rVzgvuonSwpRWpgAJWIW8SmwxfIJL8AhDyKUyKdytfL/U/KzAlb0kX4y+vdwS/QYqjTjB2XcRuId2dXjj8Wh0QxzxF/tReo8BR1jBWIKu9lFkSwR2sArgop6bAHWqz+B1+0eX7XMi4YAU+Ll8EtaaI8FTE0HVG1UBXV4lLw3eOeI12UNx26YuNvfJCKo+kI3CY3PDqCf8pPYYdUs9FpbTtkaJDGGg44S8OilMoW4QUEYFFZiym4SvYnjCslZuHIp/Laj4StV9M40Stfwspyc7tlvxCBGU4kYViS0NG7AR8FDNElMoNiWWHPDTI3+OeH8KEhpc0fwn5aqWbMN4p4cLzVnugo7lN4HUE9r0pvkVqtmK/sn2x5i80lv77UFuEW4CDF7ijVM0xqW0vjIR46IPqBLwV+/Cyby6jSL6xg4g25ZraYD85H9SdDsmAkIb6lQmMF4Q/GJ+qWdriFuX8vypae1+nspjJ9N6ebfMBDrowdrCa3mKzDm0PJDnD+ICM7nOYCcUrceTbgAnnEVGud4rLDYeZwebVJMTicMVdvJyxC616heOmNqZ+YYcJNFDFDb+60u4LXrzkaIALMuvN+37rFdFF2Y2RjJ/BPP96PVMKuud+LZvgT4kr4K9I+6XQlIDxY5cPvBcxL8XB0pBCae13D8H/JgjOA9v2BR6jmMHqi3BXRSoYuGf8FxIzxQNge+ZT71JwiTiPC/d5sm8t0AZzhRU4qy6OTxUx5+4c1rE56EFIcO6yVqcYf7IJ'/></query></iq>";
        GetTraceInfoResponsePacket packet = PacketParserUtils.parseStanza(responsePacket);

        TraceInfo traceInfo = new TraceInfo();
        traceInfo.updateTrace(967551, 0, 1540, DataParseUtils.parseTracePoints(packet.getTraceData()));
        System.out.println(traceInfo.getPoints());
    }

    @Test
    void testParseTrace() throws Exception {
        TraceInfo traceInfo = new TraceInfo();
        String[] tracePackages = IOUtils.toString(getClass().getResourceAsStream("/tracemessages.iq"), "UTF-8").split(", ");
        System.out.println(tracePackages.length);

        System.out.print("var positions=[");
        for (String tracePackage : tracePackages) {
            TraceInfoPacket packet = PacketParserUtils.parseStanza(tracePackage);
            int traceId = packet.getTraceId();
            int tf = packet.getTf();
            int tt = packet.getTt();
            String tr = packet.getTr();

            List<TracePoint> tracePoints = DataParseUtils.parseTracePoints(tr);
            for (TracePoint tracePoint : tracePoints) {
                System.out.print("[" + tracePoint.getPositionX() + ", " + tracePoint.getPositionY() + "],");
            }

            traceInfo.updateTrace(traceId, tf, tt, tracePoints);
        }
        System.out.println("]");

        System.out.println(traceInfo);
        System.out.println(traceInfo.getPoints().size());
    }


}
