/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.action.map.RenameMapSetPacket;
import de.caterdev.vacuumclean.deebot.core.map.MapSetType;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.impl.JidCreate;

import java.util.HashMap;
import java.util.Map;

public class RenameMapSetPacketTest extends AbstractPacketTest {
    @Test
    void testParsePacket() throws Exception {
        Map<String, String> mapDetailNames=new HashMap<>();
        mapDetailNames.put("1", "Zimmer 1");
        mapDetailNames.put("2", "Zimmer 2");
        mapDetailNames.put("3", "Zimmer 3");
        mapDetailNames.put("4", "Zimmer 4");
        mapDetailNames.put("5", "Zimmer 5");

        RenameMapSetPacket packet = new RenameMapSetPacket(JidCreate.from("from"), JidCreate.from("to"), MapSetType.SpotArea,"0", mapDetailNames);

        System.out.println(packet.toXML(null).toString());

        String stanza = "<iq xmlns='jabber:client' to='to' from='from' id='wZvPt-4' type='set'><query xmlns='com:ctl'><ctl id='wZvPt-3' td='RenameM' msid='0' tp='sa'><m mid='1' n='Zimmer 1'/><m mid='2' n='Zimmer 2'/><m mid='3' n='Zimmer 3'/><m mid='4' n='Zimmer 4'/><m mid='5' n='Zimmer 5'/></ctl></query></iq>";
        packet = PacketParserUtils.parseStanza(stanza);
        System.out.println(packet.toXML(null).toString());
    }
}
