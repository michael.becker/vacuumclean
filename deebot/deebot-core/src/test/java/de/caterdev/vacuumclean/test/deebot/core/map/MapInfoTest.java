/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.core.map;

import de.caterdev.vaccumclean.deebot.smack.packets.info.MapPInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.GetMapModelResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.PullMapPieceResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapSetResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.PullMResponsePacket;
import de.caterdev.vacuumclean.deebot.core.DataParseUtils;
import de.caterdev.vacuumclean.deebot.core.map.*;
import de.caterdev.vacuumclean.deebot.core.ui.CleanMapData;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.apache.commons.io.IOUtils;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapInfoTest extends AbstractPacketTest {
    public static final String MAP_MODEL_PACKET = "<iq to='to' type='set' id='0DE5BFF098A0'><query xmlns='com:ctl'><ctl id='W3jcB-11' i='1675619197' w='100' h='100' r='8' c='8' p='50' m='1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,405855736,303969177,369753284,3855924242,1295764014,1295764014,1295764014,1295764014,815663441,2763375989,1263960677,1295764014,1295764014,1295764014,1295764014,1295764014,3480335780,85136059,3084431172,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014'/></query></iq>";

    public static final String PULL_MP_0 = "<iq to='to' type='set' id='05A85E88B420'><query xmlns='com:ctl'><ctl id='u1wZR-11' ret='ok' i='1675619197' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kHoLTL8U6PAFLsX7Jhrz0KgA='/></query></iq>";

    @Test
    void testDecodeMapPieceData() throws Exception {
        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAP_MODEL_PACKET)).getMapModel();
        String data = "XQAABAAQJwAAAADuBErhGrG1aQvAhg3Yt6mexlNWb9z6yBRpaNAFdCxq/iwUejEnws/QH+l+yd658uE4d1/5KuLvTNPQfkECp5dfyu1etjZlNBcXEOvmZpnKj3nnDj7iL7zZOIuHVAD+cGUVOlXSo8GTi3aWjPZy0E2x4seD0Dg7rN6v7ZKwzFenRIzZJZ5vQccbNv4v7y1Xz3jgn24qHXCXeeRUHrdQSbnTwcNbpm28xtMM++9oSEI8XmFSyM12OWHHzQ64SlCofTNbNGE1ylpQWeEvjGfDUsNpFPqZAS0b28iAvLwm0fGX3asMAAuwJg2fPO4lBhjOeenYG+Z5k95e7niNOdyqtDHV/aAfUlAqI4R3fEJ1yqHO2Mo1G5xiqNsIPqMHZuMLcHUhp+JkdFWqxIudHti9wXjU3Qo3XJGOA3zneYh+7mfGHwsEbukIGnv/TpMDNnvXD9prz4eCIQgyP6kejuf38GI36PFN8tMeMMU8IGypvN6ONIsA";

        byte[] decodedData = DataParseUtils.decode7zData(data);
        assertEquals(mapModel.getPieceDataSize(), decodedData.length);


        MapInfo mapInfo = new MapInfo(mapModel);

        String[] mapPPackages = IOUtils.toString(getClass().getResourceAsStream("/mappmessages.iq"), "UTF-8").split(",");


        for (String mapPPackage : mapPPackages) {
            MapPInfoPacket packet = PacketParserUtils.parseStanza(mapPPackage);
            MapPiece mapPiece = packet.getMapPiece();

            mapInfo.updateMapBuffer(mapPiece);
        }


        mapInfo.writeUnscaledHTML("target/canvasreal.html");

        CleanMapData cleanMapData = new CleanMapData();
        System.out.println(cleanMapData);
        cleanMapData.setMapInfo(mapInfo);
        System.out.println(cleanMapData);
        cleanMapData.updateScale(800, 600);
        System.out.println(cleanMapData);
        cleanMapData.writeHTML("target/canvasscaled.html", 600, 600, false, false);
    }

    @Test
    void testInitWithMapModel() throws Exception {
        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAP_MODEL_PACKET)).getMapModel();
        MapPiece mapPiece0 = ((PullMapPieceResponsePacket) PacketParserUtils.parseStanza(PULL_MP_0)).getMapPiece();

        System.out.println(mapModel.getCrc().length);

        System.out.println(mapPiece0.getChecksum());
        System.out.println(mapPiece0.getData().length);
        System.out.println(Arrays.toString(mapPiece0.getData()));

        MapInfo mapInfo = new MapInfo(mapModel);
        System.out.println(mapModel);

        for (int i = 0; i < mapModel.getCrc().length; i++) {
            System.out.println(i + ": " + mapModel.getCrc()[i] + "\t" + mapInfo.getChecksum(i));
        }
    }

    @Test
    void testReadAndWriteMap() throws Exception {
        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAP_MODEL_PACKET)).getMapModel();
        MapInfo mapInfo = MapInfo.readMapData(getClass().getResource("/deebot.map").getFile(), mapModel);

        System.out.println(mapInfo);
        mapInfo.writeUnscaledHTML("target/deebot.map.html");

        CleanMapData cleanMapData = new CleanMapData();
        cleanMapData.setMapInfo(mapInfo);
        // Position(angle=-106, x=2017.0, y=1934.0, originalX=170.0, originalY=-668.0), phoneX: 0.0, phoneY: 0.0
        cleanMapData.setBotPosition(new Position(-106, 170, -668));
        // Position(angle=106, x=2016.0, y=1933.0, originalX=169.0, originalY=-670.0), phoneX: 0.0, phoneY: 0.0
        cleanMapData.setChargerPosition(new Position(106, 169, -670));

        GetMapSetResponsePacket mapSetResponsePacket = PacketParserUtils.parseStanza("<iq xmlns='jabber:client' to='username23@ecouser.net/resource' from='e0001057017609483161@115.ecorobot.net/atom' id='0F35BE4EC7B0' type='set'><query xmlns='com:ctl'><ctl id='pF2te-99' ret='ok' msid='7' tp='sa'><m mid='0' p='1'/><m mid='1' p='1'/><m mid='2' p='1'/><m mid='3' p='1'/><m mid='4' p='1'/><m mid='5' p='1'/></ctl></query></iq>");

        String pullM0 = "<iq to='username23@ecouser.net/resource' type='set' id='0C00E5753390'><query xmlns='com:ctl'><ctl id='lDAhL-11' ret='ok' m='-3300,-550;-3300,-350;-3150,-100;-3150,300;-2950,450;-3050,550;-3000,1100;-2800,1250;-2950,1400;-2600,2250;-2650,3350;-2550,3700;-1550,3650;-1450,3550;-1550,3200;-1300,2850;-1100,3150;-1250,3300;-1250,3650;-1050,3850;-400,3600;450,3600;850,3400;850,3100;650,2800;750,2700;1100,2700;1100,-1050;850,-1050;400,-850;100,-900;-100,-750;-700,-750;-1050,-550;-1200,-550;-1450,-800;-1450,-950;-2500,-550;-3300,-550'/></query></iq>";
        String pullM1 = "<iq to='username23@ecouser.net/resource' type='set' id='0B8F2285BF30'><query xmlns='com:ctl'><ctl id='lDAhL-15' ret='ok' m='-8000,3650;-8000,4000;-7850,4250;-7850,5000;-7750,5300;-7700,5400;-7250,5600;-7050,5300;-7100,5150;-6950,4900;-6400,4700;-5350,4700;-5200,4850;-5200,5050;-5150,5050;-4650,4800;-4650,4650;-4450,4450;-4200,4350;-3750,4300;-3600,4450;-3500,4450;-2800,4150;-2750,3800;-2650,3700;-2750,3100;-2850,3050;-3300,3100;-3350,3300;-4150,3300;-4250,3200;-4900,3200;-4900,3300;-5150,3450;-6050,3750;-7400,3750;-7600,3500;-7950,3550;-8000,3650'/></query></iq>";
        String pullM2 = "<iq to='username23@ecouser.net/resource' type='set' id='0422A8248190'><query xmlns='com:ctl'><ctl id='lDAhL-19' ret='ok' m='-8950,7050;-8950,7350;-8800,7550;-8850,8150;-8550,8900;-8550,10050;-8350,10100;-8000,9950;-7150,9950;-6950,9900;-6800,9750;-6600,9850;-6450,9600;-6650,9650;-7000,9300;-7000,8700;-6800,8600;-6950,8600;-7150,8350;-7050,8150;-7100,7900;-7000,7800;-6850,7900;-6650,7850;-6700,7700;-6750,7750;-7000,7600;-7000,7500;-6700,7200;-6800,7050;-6800,6400;-6850,6350;-7100,6400;-7200,6200;-7400,6100;-7400,5950;-7250,5700;-7600,5500;-8000,5600;-8300,5450;-8500,5450;-8550,5800;-8850,6100;-8700,6450;-8700,6950;-8850,7100;-8900,7050;-8950,7050'/></query></iq>";
        String pullM3 = "<iq to='username23@ecouser.net/resource' type='set' id='0745CE236C70'><query xmlns='com:ctl'><ctl id='lDAhL-23' ret='ok' m='-6900,50;-6800,200;-6850,250;-6800,500;-6600,500;-6400,250;-6300,500;-6200,550;-5850,500;-5450,600;-5250,1150;-5300,2300;-5050,2650;-5050,3000;-4950,3100;-4300,3100;-4150,2850;-3900,2750;-3650,2750;-3850,2200;-3850,1100;-3600,950;-3600,600;-3850,250;-3850,-150;-3700,-300;-3500,-350;-3650,-550;-4750,-150;-6350,-150;-6450,250;-6650,50;-6900,50'/></query></iq>";
        String pullM4 = "<iq to='username23@ecouser.net/resource' type='set' id='086789DCF820'><query xmlns='com:ctl'><ctl id='lDAhL-27' ret='ok' m='-6200,7600;-6050,8000;-6100,8450;-5900,8900;-5850,9700;-5500,9700;-4500,9300;-3200,9300;-3150,9250;-3400,8500;-3400,7550;-3550,7400;-3850,7400;-4050,7200;-4050,6550;-3950,6450;-4100,6300;-4000,6200;-3800,6250;-3700,6150;-3700,6000;-3750,5500;-3850,5400;-4050,5500;-4250,5400;-4200,5000;-4500,5050;-4600,4900;-4700,4900;-5400,5250;-5500,5550;-5700,5650;-6000,5650;-6100,5750;-6100,6200;-6000,6300;-6050,6450;-5950,6600;-5950,6900;-5800,7100;-5800,7450;-5950,7550;-6200,7600'/></query></iq>";
        String pullM5 = "<iq to='username23@ecouser.net/resource' type='set' id='03969782CAD0'><query xmlns='com:ctl'><ctl id='lDAhL-31' ret='ok' m='-3550,4900;-3250,5050;-3150,5250;-3150,5650;-2950,5850;-2850,6100;-2850,6700;-3000,6800;-3000,7150;-2900,7450;-2250,7200;-1200,7200;-1100,7300;-1300,7450;-1250,7750;-950,7800;-800,7950;-750,8400;-600,8550;-650,8600;-600,8850;250,8500;1100,8500;1100,5350;750,5350;550,5100;600,4250;350,4200;250,4350;50,4450;-450,4450;-550,4550;-500,5150;-850,5400;-1350,5550;-2050,5550;-2200,5450;-2300,5350;-2300,4900;-2500,4700;-2750,4750;-2950,4350;-3450,4500;-3450,4800;-3550,4900'/></query></iq>";
        String[] pullMs = {pullM0, pullM1, pullM2, pullM3, pullM4, pullM5};

        for (MapDetail mapDetail : mapSetResponsePacket.getMapDetails()) {
            PullMResponsePacket pullMResponsePacket = PacketParserUtils.parseStanza(pullMs[mapDetail.getMid()]);
            mapDetail.setPositions(pullMResponsePacket.getPositions());

            if (null == cleanMapData.getMapDetails().get(mapDetail.getMapSetType())) {
                cleanMapData.getMapDetails().put(mapDetail.getMapSetType(), new ArrayList<>());
            }

            cleanMapData.getMapDetails().get(mapDetail.getMapSetType()).add(mapDetail);
        }


        cleanMapData.writeHTML("target/deebot.map.scaled.html", 800, 600, false, true);
        //cleanMapData.updateScale2(800, 600);
    }
}
