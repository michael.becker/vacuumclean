/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.AddSchedPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.GetSchedPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.DeleteSchedPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.ModSchedPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.sched.GetSchedResponsePacket;
import de.caterdev.vacuumclean.deebot.core.clean.CleanSchedule;
import de.caterdev.vacuumclean.deebot.core.constants.CleanType;
import de.caterdev.vacuumclean.deebot.core.constants.Day;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.impl.JidCreate;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GetSchedPacketTest extends AbstractPacketTest {
    public static final String id = StanzaIdUtil.newStanzaId();
    public static final String from = "from";
    public static final String to = "to";
    public static final String innerId = StanzaIdUtil.newStanzaId();

    public static final String PACKET = String.format("<iq xmlns='jabber:client' to='%s' from='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl id='%s' td='GetSched'/></query></iq>", to, from, id, innerId);

    @Test
    void testParsePacket() throws Exception {
        GetSchedPacket packet = PacketParserUtils.parseStanza(PACKET);
        assertNotNull(packet);
        assertEquals(id, packet.getStanzaId());
        assertEquals(from, packet.getFrom().asUnescapedString());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(innerId, packet.getInnerId());
    }

    @Test
    void testCreatePacket() throws Exception {
        GetSchedPacket packet = new GetSchedPacket(JidCreate.from(from), JidCreate.from(to));
        packet.setStanzaId(id);
        packet.setInnerId(innerId);

        assertEquals(from, packet.getFrom().asUnescapedString());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(IQ.Type.set, packet.getType());

        assertEquals(PACKET, packet.toXML(null).toString());
    }

    @Test
    void testParseAddSchedPacketTest() throws Exception {
        String stanza = "<iq xmlns='jabber:client' to='$to' from='$from' id='7069' type='set'><query xmlns='com:ctl'><ctl id='39409991' td='AddSched'><s n='timing_name_360006138100' o='1' h='10' m='0' r='0000000'><ctl td='Clean'><clean type='auto'/></ctl></s></ctl></query></iq>";
        AddSchedPacket packet = PacketParserUtils.parseStanza(stanza);

        System.out.println(stanza);
        System.out.println(packet.toXML(null).toString());

        CleanSchedule cleanSchedule = new CleanSchedule();
        cleanSchedule.setActive(false);
        cleanSchedule.setCleanType(CleanType.Auto);
        cleanSchedule.setHour(10);
        cleanSchedule.setMinute(50);
        cleanSchedule.setRepeatDays(Arrays.asList(Day.Monday, Day.Tuesday, Day.Wednesday, Day.Saturday));
        packet = new AddSchedPacket(JidCreate.from("$from"), JidCreate.from("$to"), cleanSchedule);
        System.out.println(packet.toXML(null).toString());

        stanza = "<iq xmlns='jabber:client' to='$to' from='$from' id='7069' type='set'><query xmlns='com:ctl'><ctl id='81718615' td='ModSched'><ModSched n='timing_name_360005616100'><s n='timing_name_360005616100' o='0' h='10' m='0' r='0000000'><ctl td='Clean'><clean type='auto'/></ctl></s></ModSched></ctl></query></iq>";
        ModSchedPacket modSchedPacket = PacketParserUtils.parseStanza(stanza);
        System.out.println(stanza);
        System.out.println(modSchedPacket.toXML(null).toString());

        List<Day> days = cleanSchedule.getRepeatDays();
        cleanSchedule.setRepeatDays(Arrays.asList(days.get(0), days.get(2)));
        modSchedPacket = new ModSchedPacket(JidCreate.from("$from"), JidCreate.from("$to"), "timing_name_360005616100", cleanSchedule);
        System.out.println(modSchedPacket.toXML(null).toString());

        stanza = "<iq id='$id' to='$to' from='$from' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='DelSched'><DelSched n='$name'/></ctl></query></iq>";
        DeleteSchedPacket deleteSchedPacket = PacketParserUtils.parseStanza(stanza);
        System.out.println(stanza);
        System.out.println(deleteSchedPacket.toXML(null).toString());

        deleteSchedPacket = new DeleteSchedPacket(JidCreate.from("$from"), JidCreate.from("$to"), "originalName");
        System.out.println(deleteSchedPacket.toXML(null).toString());
    }

    @Test
    void testParseGetSchedResponsePacket() throws Exception {
        String stanza = "<iq xmlns='jabber:client' to='$to' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok'><s n='timing_name_360006138100' o='0' h='10' m='0' r='0000000' f='p'><ctl td='Clean'><clean type='auto'/></ctl></s><s n='timing_name_36000231100' o='1' h='10' m='0' r='0000000' f='q'><ctl td='Clean'><clean type='auto'/></ctl></s><s n='timing_name_360006179100' o='1' h='10' m='0' r='0000000' f='q'><ctl td='Clean'><clean type='auto'/></ctl></s></ctl></query></iq>";
        GetSchedResponsePacket packet = PacketParserUtils.parseStanza(stanza);

        System.out.println(stanza);
        System.out.println(packet.toXML(null).toString());
    }
}
