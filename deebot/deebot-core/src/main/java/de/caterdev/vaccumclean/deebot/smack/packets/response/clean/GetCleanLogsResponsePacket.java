/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response.clean;

import de.caterdev.vaccumclean.deebot.smack.packets.response.ResponsePacket;
import de.caterdev.vacuumclean.deebot.core.clean.CleanStatistics;
import de.caterdev.vacuumclean.deebot.core.constants.CleanStartReason;
import de.caterdev.vacuumclean.deebot.core.constants.CleanStopReason;
import lombok.Getter;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$id' ret='ok'><CleanSt a='2' s='1564092832' l='43' t='a' f='a'/><CleanSt a='58' s='1563472545' l='3636' t='b' f='s'/></ctl></query></iq>
 */
public class GetCleanLogsResponsePacket extends ResponsePacket {
    public static final String ELEMENT_CLEAN_STATISTIC = "CleanSt";
    public static final String ATTRIBUTE_CLEANED_AREA = "a";
    public static final String ATTRIBUTE_TIMESTAMP_START = "s";
    public static final String ATTRIBUTE_LAST_TIME = "l";
    public static final String ATTRIBUTE_START_REASON = "t";
    public static final String ATTRIBUTE_STOP_REASON = "f";

    @Getter
    private final List<CleanStatistics> cleanStatistics = new ArrayList<>();

    public GetCleanLogsResponsePacket(String innerId, RetValue retValue, XmlPullParser parser) throws Exception {
        super(innerId, retValue, false, true);

        while (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals(ELEMENT_CLEAN_STATISTIC)) {
            CleanStatistics cleanStatistics = new CleanStatistics();
            cleanStatistics.setCleanedArea(Integer.valueOf(parser.getAttributeValue(null, ATTRIBUTE_CLEANED_AREA)));
            cleanStatistics.setStartCleanTimestamp(Long.valueOf(parser.getAttributeValue(null, ATTRIBUTE_TIMESTAMP_START)));
            cleanStatistics.setLastTime(Long.valueOf(parser.getAttributeValue(null, ATTRIBUTE_LAST_TIME)));
            cleanStatistics.setCleanStartReason(CleanStartReason.get(parser.getAttributeValue(null, ATTRIBUTE_START_REASON)));
            cleanStatistics.setCleanStopReason(CleanStopReason.get(parser.getAttributeValue(null, ATTRIBUTE_STOP_REASON)));

            this.cleanStatistics.add(cleanStatistics);
            parser.nextTag();
            parser.nextTag();
        }
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        for (CleanStatistics cleanStatistics : this.cleanStatistics) {
            xml.halfOpenElement(ELEMENT_CLEAN_STATISTIC);
            xml.attribute(ATTRIBUTE_CLEANED_AREA, cleanStatistics.getCleanedArea());
            xml.attribute(ATTRIBUTE_TIMESTAMP_START, String.valueOf(cleanStatistics.getStartCleanTimestamp()));
            xml.attribute(ATTRIBUTE_LAST_TIME, String.valueOf(cleanStatistics.getLastTime()));
            xml.attribute(ATTRIBUTE_START_REASON, cleanStatistics.getCleanStartReason().getReason());
            xml.attribute(ATTRIBUTE_STOP_REASON, cleanStatistics.getCleanStopReason().getReason());
            xml.closeEmptyElement();
        }

        return xml;
    }
}
