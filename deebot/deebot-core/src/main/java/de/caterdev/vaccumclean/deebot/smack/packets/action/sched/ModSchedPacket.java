/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action.sched;

import de.caterdev.vaccumclean.deebot.smack.packets.response.SimpleResponsePacket;
import de.caterdev.vacuumclean.deebot.core.clean.CleanSchedule;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;
import org.xmlpull.v1.XmlPullParser;

/**
 * <iq id='$id' to='$to' from='$from' type='set'><query xmlns='com:ctl'><ctl id='81718615' td='ModSched'><ModSched n='timing_name_360005616100'><s n='timing_name_360005616100' o='0' h='10' m='0' r='0000000'><ctl td='Clean'><clean type='auto'/></ctl></s></ModSched></ctl></query></iq>
 */
public class ModSchedPacket extends AbstractSchedPacket<SimpleResponsePacket> {
    public static final String ACTION = "ModSched";

    public static final String ELEMENT_MOD_SCHED = "ModSched";

    private final String originalName;

    public ModSchedPacket(String innerId, XmlPullParser parser) {
        super(innerId, parser);

        try {
            parser.nextTag();
            System.out.println(parser.getName());
            this.originalName = parser.getAttributeValue(null, ATTRIBUTE_NAME);
        } catch (final Exception e) {
            throw new RuntimeException("Could not parse " + parser);
        }

        parseContents();
    }

    public ModSchedPacket(Jid from, Jid to, String originalName, CleanSchedule cleanSchedule) {
        super(from, to, cleanSchedule);

        this.originalName = originalName;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.halfOpenElement(ELEMENT_MOD_SCHED);
        xml.attribute(ATTRIBUTE_NAME, originalName);
        xml.rightAngleBracket();

        super.getAdditionalElements(xml);

        xml.closeElement(ELEMENT_MOD_SCHED);

        return xml;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
