/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets;

import de.caterdev.vaccumclean.deebot.smack.packets.action.*;
import de.caterdev.vaccumclean.deebot.smack.packets.action.admin.*;
import de.caterdev.vaccumclean.deebot.smack.packets.action.clean.GetCleanLogsPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.action.clean.GetCleanSumPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.*;
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.*;
import de.caterdev.vaccumclean.deebot.smack.packets.info.*;
import de.caterdev.vaccumclean.deebot.smack.packets.info.bot.DustCaseInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.info.map.MapSetInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.*;
import de.caterdev.vaccumclean.deebot.smack.packets.response.admin.GetWaterBoxInfoResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.admin.GetWaterPermeabilityResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.clean.GetCleanLogsResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.clean.GetCleanSumResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapBuildStateResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapSetResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.PullMResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.sched.GetSchedResponsePacket;
import de.caterdev.vacuumclean.deebot.core.UserInfo;
import lombok.extern.java.Log;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * <iq to='$RECEIVER' from='$SENDER' type='[set|get]' id='$id'>
 * <query xmlns='com:ctl'>
 * <ctl [id|ts]='$innerId' td='$command' $ADDITIONAL_ATTRIBUTES>
 * $ADDITIONAL_ELEMENTS
 * </ctl>
 * </query>
 * </iq>
 */
@Log
public abstract class Query extends IQ {
    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "com:ctl";

    public static final String ELEMENT_CTL = "ctl";

    public static final String ATTRIBUTE_ID = "id";
    public static final String ATTRIBUTE_TS = "ts";
    public static final String ATTRIBUTE_TD = "td";
    public static final String ATTRIBUTE_RET = "ret";

    protected String innerId;
    protected boolean response;
    protected boolean idAttribute;
    protected boolean additionalAttributes;
    protected boolean additionalElements;


    /**
     * @param innerId
     * @param response
     * @param idAttribute
     * @param additionalAttributes if true: subclass must override {@link #getAdditionalAttributes()}
     * @param additionalElements   if true: subclasses must override {@link #getAdditionalElements(XmlStringBuilder)}
     */
    public Query(String innerId, boolean response, boolean idAttribute, boolean additionalAttributes, boolean additionalElements) {
        super(ELEMENT, NAMESPACE);

        this.innerId = innerId;
        this.response = response;
        this.additionalElements = additionalElements;
        this.additionalAttributes = additionalAttributes;
        this.idAttribute = idAttribute;
    }

    public String getInnerId() {
        return innerId;
    }

    public void setInnerId(String innerId) {
        this.innerId = innerId;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();
        xml.halfOpenElement(ELEMENT_CTL);
        xml.append(getCtlAttributes(new XmlStringBuilder()));

//        if (idAttribute) {
//            xml.attribute(ATTRIBUTE_ID, getInnerId());
//        }


        if (additionalAttributes) {
            Map<String, String> addAttributes = getAdditionalAttributes();
            for (String attributeName : addAttributes.keySet()) {
                xml.attribute(attributeName, addAttributes.get(attributeName));
            }
        }

        if (additionalElements) {
            xml.rightAngleBracket();
            xml.append(getAdditionalElements(new XmlStringBuilder()));
            xml.closeElement(ELEMENT_CTL);
        } else {
            xml.closeEmptyElement();
        }

        return xml;
    }

    protected abstract XmlStringBuilder getCtlAttributes(XmlStringBuilder xml);

    protected Map<String, String> getAdditionalAttributes() {
        if (additionalAttributes) {
            throw new RuntimeException("Overwrite getAdditionalAttributes()!");
        }
        return new HashMap<>();
    }

    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        if (additionalElements) {
            throw new RuntimeException("Overwrite getAdditionalElements()!");
        }
        return null;
    }

    public static class Provider extends IQProvider<Query> {
        @Override
        public Query parse(XmlPullParser parser, int initialDepth) throws Exception {
            int currentParserPosition = parser.next();
            if (currentParserPosition == XmlPullParser.START_TAG && parser.getName().equals(ELEMENT_CTL)) {
                String ctlId = parser.getAttributeValue(null, ATTRIBUTE_ID);
                String tsValue = parser.getAttributeValue(null, ATTRIBUTE_TS);
                String tdValue = parser.getAttributeValue(null, ATTRIBUTE_TD);
                String retValue = parser.getAttributeValue(null, ATTRIBUTE_RET);

                if (null != tdValue) {  // Action & Info Packets
                    switch (tdValue) {
                        case PlaySoundPacket.ACTION: {
                            int soundId = Integer.valueOf(parser.getAttributeValue(null, PlaySoundPacket.ATTRIBUTE_SOUND_ID));

                            return new PlaySoundPacket(ctlId, soundId);
                        }
                        case GetBatteryInfoPacket.ACTION: {
                            return new GetBatteryInfoPacket(ctlId);
                        }
                        case GetTimePacket.ACTION: {
                            return new GetTimePacket(ctlId);
                        }
                        case ChargePacket.ACTION: {
                            parser.nextToken();

                            ChargePacket.ChargeAction chargeAction = ChargePacket.ChargeAction.valueOf(parser.getAttributeValue(null, ChargePacket.ATTRIBUTE_CHARGE_ACTION));

                            return new ChargePacket(ctlId, chargeAction);
                        }
                        case CleanPacket.ACTION: {
                            parser.nextToken();
                            String type = parser.getAttributeValue(null, CleanPacket.ATTRIBUTE_TYPE);
                            String speed = parser.getAttributeValue(null, CleanPacket.ATTRIBUTE_SPEED);
                            String act = parser.getAttributeValue(null, CleanPacket.ATTRIBUTE_ACT);
                            String mids = parser.getAttributeValue(null, CleanPacket.ATTRIBUTE_MIDS);

                            return new CleanPacket(ctlId, type, speed, act, mids);
                        }
                        case GetChargeStatePacket.ACTION: {
                            return new GetChargeStatePacket(ctlId);
                        }
                        case GetDeviceInfoPacket.ACTION: {
                            return new GetDeviceInfoPacket(ctlId);
                        }
                        case BatteryInfoPacket.INFO: {
                            parser.nextToken();
                            String power = parser.getAttributeValue(null, BatteryInfoPacket.ATTRIBUTE_POWER);

                            return new BatteryInfoPacket(tsValue, power);
                        }
                        case GetMapModelPacket.ACTION: {
                            return new GetMapModelPacket(ctlId);
                        }
                        case GetMapSetPacket.ACTION: {
                            return new GetMapSetPacket(ctlId, parser);
                        }
                        case GetTraceModelPacket.ACTION: {
                            return new GetTraceModelPacket(ctlId);
                        }
                        case GetSchedPacket.ACTION: {
                            return new GetSchedPacket(ctlId);
                        }
                        case PullMapPiecePacket.ACTION: {
                            int pid = Integer.valueOf(parser.getAttributeValue(null, PullMapPiecePacket.ATTRIBUTE_PID));

                            return new PullMapPiecePacket(ctlId, pid);
                        }
                        case DeviceInfoPacket.INFO: {
                            parser.nextTag();
                            parser.next();
                            String mid = parser.getText();

                            parser.nextTag();
                            parser.nextTag();
                            parser.next();
                            String className = parser.getText();

                            parser.nextTag();
                            parser.nextTag();
                            String v = parser.getAttributeValue(null, DeviceInfoPacket.ATTRIBUTE_V);

                            parser.nextTag();
                            parser.nextTag();
                            parser.next();
                            String wd = parser.getText();

                            return new DeviceInfoPacket(JidCreate.from(mid), className, v, wd);
                        }
                        case PosInfoPacket.INFO: {
                            return new PosInfoPacket(ctlId, parser);
                        }
                        case ChargeStateInfoPacket.INFO: {
                            parser.nextTag();

                            String chargeType = parser.getAttributeValue(null, ChargeStateInfoPacket.ATTRIBUTE_TYPE);
                            String chargeGoingReason = parser.getAttributeValue(null, ChargeStateInfoPacket.ATTRIBUTE_R);
                            String isNeedHelp = parser.getAttributeValue(null, ChargeStateInfoPacket.ATTRIBUTE_H);
                            String isContinueClean = parser.getAttributeValue(null, ChargeStateInfoPacket.ATTRIBUTE_S);

                            return new ChargeStateInfoPacket(ctlId, tsValue, chargeType, chargeGoingReason, isNeedHelp, isContinueClean);
                        }
                        case MapPInfoPacket.INFO: {
                            String i = parser.getAttributeValue(null, MapPInfoPacket.ATTRIBUTE_MAP_ID);
                            String pid = parser.getAttributeValue(null, MapPInfoPacket.ATTRIBUTE_PID);
                            String p = parser.getAttributeValue(null, MapPInfoPacket.ATTRIBUTE_DATA);

                            return new MapPInfoPacket(ctlId, i, pid, p);
                        }
                        case TrMInfoPacket.INFO: {
                            String traceId = parser.getAttributeValue(null, TrMInfoPacket.ATTRIBUTE_TRACE_ID);
                            String count = parser.getAttributeValue(null, TrMInfoPacket.ATTRIBUTE_COUNT);

                            return new TrMInfoPacket(ctlId, traceId, count);
                        }
                        case TraceInfoPacket.INFO: {
                            String traceId = parser.getAttributeValue(null, TraceInfoPacket.ATTRIBUTE_TRACE_ID);
                            String tf = parser.getAttributeValue(null, TraceInfoPacket.ATTRIBUTE_TF);
                            String tt = parser.getAttributeValue(null, TraceInfoPacket.ATTRIBUTE_TT);
                            String tr = parser.getAttributeValue(null, TraceInfoPacket.ATTRIBUTE_TR);

                            return new TraceInfoPacket(ctlId, traceId, tf, tt, tr);
                        }
                        case GetUserInfoPacket.ACTION: {
                            return new GetUserInfoPacket(ctlId);
                        }
                        case AddUserPacket.ACTION: {
                            String to = parser.getAttributeValue(null, AddUserPacket.ATTRIBUTE_NEW_CLIENT);

                            return new AddUserPacket(ctlId, JidCreate.from(to));
                        }
                        case DelUserPacket.ACTION: {
                            String user = parser.getAttributeValue(null, DelUserPacket.ATTRIBUTE_USER);

                            return new DelUserPacket(ctlId, JidCreate.from(user));
                        }
                        case AddSchedPacket.ACTION: {
                            return new AddSchedPacket(ctlId, parser);
                        }
                        case ModSchedPacket.ACTION: {
                            return new ModSchedPacket(ctlId, parser);
                        }
                        case DeleteSchedPacket.ACTION: {
                            return new DeleteSchedPacket(ctlId, parser);
                        }
                        case UserDeledPackage.INFO: {
                            String user = parser.getAttributeValue(null, UserDeledPackage.ATTRIBUTE_USER);

                            return new UserDeledPackage(ctlId, JidCreate.from(user));
                        }
                        case GetAccessControlAction.ACTION: {
                            return new GetAccessControlAction(ctlId);
                        }
                        case GetDCStatusPacket.ACTION: {
                            return new GetDCStatusPacket(ctlId);
                        }
                        case SetMapAutoReportPacket.ACTION: {
                            String autoReport = parser.getAttributeValue(null, SetMapAutoReportPacket.ATTRIBUTE_ON);

                            return new SetMapAutoReportPacket(ctlId, autoReport);
                        }
                        case GetWKVerPacket.ACTION: {
                            return new GetWKVerPacket(ctlId);
                        }
                        case WKVerResponsePacket.RESPONSE: {
                            parser.nextTag();
                            String fwVer = parser.nextText();

                            return new WKVerResponsePacket(ctlId, fwVer);
                        }
                        case GetActiveLanguagePacket.ACTION: {
                            return new GetActiveLanguagePacket(ctlId);
                        }
                        case GetBlockTimePacket.ACTION: {
                            return new GetBlockTimePacket(ctlId);
                        }
                        case GetChargerPosPacket.ACTION: {
                            return new GetChargerPosPacket(ctlId);
                        }
                        case GetPosPacket.ACTION: {
                            return new GetPosPacket(ctlId);
                        }
                        case SetACPacket.ACTION: {
                            String to = parser.getAttributeValue(null, SetACPacket.ATTRIBUTE_NEW_CLIENT);

                            // TODO: wir erlauben an dieser Stelle alles
                            return new SetACPacket(ctlId, JidCreate.from(to), true, true, true);
                        }
                        case GetTraceInfoPacket.ACTION: {
                            return new GetTraceInfoPacket(ctlId, parser);
                        }
                        case GetMapBuildStatePacket.ACTION: {
                            return new GetMapBuildStatePacket(ctlId);
                        }
                        case GetCleanLogsPacket.ACTION: {
                            return new GetCleanLogsPacket(ctlId, parser);
                        }
                        case GetCleanSumPacket.ACTION: {
                            return new GetCleanSumPacket(ctlId);
                        }
                        case PullMPacket.ACTION: {
                            return new PullMPacket(ctlId, parser);
                        }
                        case ClearMapPacket.ACTION: {
                            return new ClearMapPacket(ctlId);
                        }
                        case SetTimePacket.ACTION: {
                            return new SetTimePacket(ctlId, parser);
                        }
                        case UserInfosPacket.INFO: {
                            Collection<UserInfo> userInfos = new ArrayList<>();
                            while (parser.nextTag() == XmlPullParser.START_TAG && parser.getName().equals(UserInfosPacket.ELEMENT_USER)) {
                                Jid jid = JidCreate.from(parser.getAttributeValue(null, UserInfosPacket.ATTRIBUTE_JID));
                                String rights = parser.getAttributeValue(null, UserInfosPacket.ATTRIBUTE_RIGHTS);

                                userInfos.add(new UserInfo(jid, rights));
                                parser.nextTag();
                            }

                            return new UserInfosPacket(ctlId, userInfos.toArray(new UserInfo[userInfos.size()]));
                        }
                        case SleepStatusInfoPacket.INFO: {
                            return new SleepStatusInfoPacket(ctlId, parser);
                        }
                        case CleanStatusInfoPacket.INFO: {
                            return new CleanStatusInfoPacket(ctlId, parser);
                        }
                        case CleanReportInfoPacket.INFO: {
                            return new CleanReportInfoPacket(ctlId, parser);
                        }
                        case MapBuildStateInfoPacket.INFO: {
                            return new MapBuildStateInfoPacket(tsValue, parser);
                        }
                        case MapSetInfoPacket.INFO: {
                            return new MapSetInfoPacket(tsValue, parser);
                        }
                        case DustCaseInfoPacket.INFO: {
                            return new DustCaseInfoPacket(tsValue, parser);
                        }
                        case ErrorInfoPacket.INFO: {
                            return new ErrorInfoPacket(tsValue, parser);
                        }
                        case RenameMapSetPacket.ACTION: {
                            return new RenameMapSetPacket(ctlId, parser);
                        }
                        case GetResourcePacket.ACTION: {
                            return new GetResourcePacket(ctlId);
                        }
                        case IfHaveMapPacket.ACTION: {
                            return new IfHaveMapPacket(ctlId);
                        }
                        case GetNetInfoPacket.ACTION: {
                            return new GetNetInfoPacket(ctlId);
                        }
                        case GetNetInfoFromFWPacket.ACTION: {
                            return new GetNetInfoFromFWPacket(ctlId);
                        }
                        case GetSysInfoPacket.ACTION: {
                            return new GetSysInfoPacket(ctlId);
                        }
                        case GetUARTInfoPacket.ACTION: {
                            return new GetUARTInfoPacket(ctlId);
                        }
                        case GetWaterPermeabilityPacket.ACTION: {
                            return new GetWaterPermeabilityPacket(ctlId);
                        }
                        case GetWaterBoxInfoPacket.ACTION: {
                            return new GetWaterBoxInfoPacket(ctlId);
                        }
                        default: {
                            return new GenericActionPacket(ctlId, tdValue);
                        }
                    }
                } else if (null != retValue) { // Response Packets
                    switch (ResponsePacket.RetValue.valueOf(retValue)) {
                        case fail: {
                            String errno = parser.getAttributeValue(null, ErrorResponsePacket.ATTRIBUTE_ERRNO);
                            String errorMessage = parser.getAttributeValue(null, ErrorResponsePacket.ATTRIBUTE_ERROR);

                            return new ErrorResponsePacket(ctlId, errno, errorMessage);
                        }
                        case ok: {
                            if (GetPosResponsePacket.isPacket(parser)) {
                                return new GetPosResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), parser);
                            }
                            if (PullMapPieceResponsePacket.isPacket(parser)) {
                                return new PullMapPieceResponsePacket(ctlId, parser);
                            }
                            if (GetTimeResponsePacket.isPacket(parser)) {
                                return new GetTimeResponsePacket(ctlId, parser);
                            }
                            if (GetWaterPermeabilityResponsePacket.isPacket(parser)) {
                                return new GetWaterPermeabilityResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), parser);
                            }
                            if (GetWaterBoxInfoResponsePacket.isPacket(parser)) {
                                return new GetWaterBoxInfoResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), parser);
                            }

                            switch (ResponsePacketType.getType(parser)) {
                                case traceInfoResponsePacket:
                                    return new GetTraceInfoResponsePacket(ctlId, parser);
                                case traceModelResponsePacket:
                                    return new GetTraceModelResponsePacket(ctlId, parser);
                                case mapBuildStateResponsePacket:
                                    return new GetMapBuildStateResponsePacket(ctlId, parser);
                                case mapSetResponsePacket:
                                    return new GetMapSetResponsePacket(ctlId, parser);
                                case cleanSumResponsePacket:
                                    return new GetCleanSumResponsePacket(ctlId, parser);
                                case activeLanguageResponsePacket:
                                    String lang = parser.getAttributeValue(null, GetActiveLanguageResponsePacket.ATTRIBUTE_LANGUAGE);
                                    return new GetActiveLanguageResponsePacket(ctlId, ResponsePacket.RetValue.ok, lang);
                                case pullMResponsePacket:
                                    return new PullMResponsePacket(ctlId, parser);
                                default:
                                    break;
                            }

                        }
                    }

                    parser.nextToken();
                    // Generic Response without additional information
                    if (parser.getEventType() == XmlPullParser.END_TAG) {
                        return new SimpleResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue));
                    }

                    switch (parser.getName()) {
                        case GetBatteryInfoResponsePacket.ELEMENT_BATTERY: {
                            String power = parser.getAttributeValue(null, GetBatteryInfoResponsePacket.ATTRIBUTE_POWER);
                            return new GetBatteryInfoResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), power);
                        }
                        case GetChargeStateResponsePacket.ELEMENT_CHARGE: {
                            String chargeType = parser.getAttributeValue(null, GetChargeStateResponsePacket.ATTRIBUTE_CHARGE_TYPE);
                            return new GetChargeStateResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), GetChargeStateResponsePacket.ChargeType.valueOf(chargeType));
                        }
                        case GetBlockTimeResponsePacket.ELEMENT_BLOCKTIME: {
                            String startHour = parser.getAttributeValue(null, GetBlockTimeResponsePacket.ATTRIBUTE_START_HOUR);
                            String startMinute = parser.getAttributeValue(null, GetBlockTimeResponsePacket.ATTRIBUTE_START_MINUTE);
                            String endHour = parser.getAttributeValue(null, GetBlockTimeResponsePacket.ATTRIBUTE_END_HOUR);
                            String endMinute = parser.getAttributeValue(null, GetBlockTimeResponsePacket.ATTRIBUTE_END_MINUTE);

                            return new GetBlockTimeResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), startHour, startMinute, endHour, endMinute);
                        }
                        case GetCleanLogsResponsePacket.ELEMENT_CLEAN_STATISTIC: {
                            return new GetCleanLogsResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), parser);
                        }
                        case AbstractSchedPacket.ELEMENT_SETTINGS: {
                            return new GetSchedResponsePacket(ctlId, ResponsePacket.RetValue.valueOf(retValue), parser);
                        }
                    }
                } else if (GetMapModelResponsePacket.isPacket(parser)) {
                    return new GetMapModelResponsePacket(ctlId, parser);
                }
            }

            log.warning(String.format("unparsable: %s, %s", parser.getName(), parser.getText()));

            return null;
        }
    }
}
