/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response.map;

import de.caterdev.vaccumclean.deebot.smack.packets.response.ResponsePacket;
import de.caterdev.vacuumclean.deebot.core.map.MapDetail;
import de.caterdev.vacuumclean.deebot.core.map.MapSetType;
import lombok.Getter;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok' tp='$mapSetType' msid='$mapSetId'/></query></iq>
 * <iq to='$tp' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok' tp='$mapSetType' msid='$mapSetId'><m mid='0' p='1'/><m mid='1' p='1'/><m mid='2' p='1'/><m mid='3' p='1'/><m mid='4' p='1'/><m mid='5' p='1'/></ctl></query></iq>
 */
public class GetMapSetResponsePacket extends ResponsePacket {
    public static final String ATTRIBUTE_MAP_SET_TYPE = "tp";
    public static final String ATTRIBUTE_MAP_SET_ID = "msid";

    public static final String ELEMENT_MAP_DETAIL = "m";
    public static final String ATTRIBUTE_MAP_DETAIL_ID = "mid";
    public static final String ATTRIBUTE_MAP_DETAIL_P = "p";
    public static final String ATTRIBUTE_MAP_DETAIL_NAME = "n";

    @Getter
    private final MapSetType mapSetType;
    @Getter
    private final String mapSetId;
    @Getter
    private final List<MapDetail> mapDetails;

    public GetMapSetResponsePacket(String innerId, XmlPullParser parser) throws Exception {
        super(innerId, parser, true, true);

        this.mapSetType = MapSetType.get(parser.getAttributeValue(null, ATTRIBUTE_MAP_SET_TYPE));
        this.mapSetId = parser.getAttributeValue(null, ATTRIBUTE_MAP_SET_ID);
        this.mapDetails = new ArrayList<>();

        while (parser.nextTag() == XmlPullParser.START_TAG && parser.getName().equals(ELEMENT_MAP_DETAIL)) {
            MapDetail mapDetail = new MapDetail();
            mapDetail.setMapSetType(mapSetType);
            mapDetail.setMid(Integer.valueOf(parser.getAttributeValue(null, ATTRIBUTE_MAP_DETAIL_ID)));
            mapDetail.setP(Integer.valueOf(parser.getAttributeValue(null, ATTRIBUTE_MAP_DETAIL_P)));
            mapDetail.setName(parser.getAttributeValue(null, ATTRIBUTE_MAP_DETAIL_NAME));

            mapDetails.add(mapDetail);

            parser.nextTag();
        }
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> attributes = new HashMap<>();
        attributes.put(ATTRIBUTE_MAP_SET_TYPE, mapSetType.getType());
        attributes.put(ATTRIBUTE_MAP_SET_ID, mapSetId);

        return attributes;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        for (MapDetail mapDetail : mapDetails) {
            xml.halfOpenElement(ELEMENT_MAP_DETAIL);
            xml.attribute(ATTRIBUTE_MAP_DETAIL_ID, mapDetail.getMid());
            xml.attribute(ATTRIBUTE_MAP_DETAIL_P, mapDetail.getP());

            if (null != mapDetail.getName()) {
                xml.attribute(ATTRIBUTE_MAP_DETAIL_NAME, mapDetail.getName());
            }
            xml.closeEmptyElement();
        }
        return xml;
    }
}
