/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action;

import org.jxmpp.jid.Jid;

import java.util.HashMap;
import java.util.Map;

/**
 * <code>
 * <iq id="$ID" to="$TO" from="$FROM" type="set">
 * <query xmlns="com:ctl">
 * <ctl id="$ID" td="PlaySound" sid="$SOUND_ID"/>
 * </query>
 * </iq>
 * </code>
 *
 * <ul>
 * <li>0 ... Einschaltgeräusch</li>
 * <li>3 ... Roboter ist ausgeschaltet</li>
 * <li>4 ... Überprüfen Sie die Antriebsräder</li>
 * <li>5 ... Ich brauche Hilfe</li>
 * <li>6 ... Setzen Sie den Staubbehälter ein</li>
 * <li>17 ... Ping-Geräusch</li>
 * <li>18 ... Der Akkustand ist niedrig</li>
 * <li>29 ... Vor dem Laden bitte einschalten</li>
 * <li>30 ... Roboter befindet sich hier</li>
 * <li>31 ... Bürste hat sich verknotet, reinigen Sie die Bürste</li>
 * <li>35 ... Reinigen Sie die Absturzsensoren</li>
 * <li>48 ... Bürste hat sich verknotet, reinigen Sie die Bürste</li>
 * <li>55 ... neue Position wird gesucht</li>
 * <li>56 ... Aktualisierung erfolgreich</li>
 * <li>63 ... Rückkehr zur Ladestation</li>
 * <li>65 ... Reinigung pausiert</li>
 * <li>69 ... Verbunden. Gehen Sie zurück zur Ecovacs-App, um die Einrichtung fortzusetzen</li>
 * <li>71 ... Die Karte wird wieder hergestellt. Bitte nicht seitlich ans Gerät stellen.</li>
 * <li>73 ... Der Akkustand ist niedrig. Rückkehr zur Ladestation</li>
 * <li>74 ... Position ist schwer zu ermitteln. Neuer Reinigungsvorgang wird gestartet</li>
 * <li>75 ... Reinigung wird fortgesetzt</li>
 * <li>76 ... Aktualisierung fehlgeschlagen. Bitte versuchen Sie es erneut.</li>
 * <li>77 ... Bitte in die Ladestation einsetzen.</li>
 * <li>79 ... Reinigung wird fortgesetzt</li>
 * <li>80 ... Reinigung wird gestartet</li>
 * <li>81 ... Reinigung wird gestartet</li>
 * <li>82 ... Reinigung wird gestartet</li>
 * <li>84 ... Bereit für den Wischvorgang</li>
 * <li>85 ... Entfernen Sie die Wischplatte, während ich die Karte erstelle</li>
 * <li>86 ... Reinigung ist abgeschlossen. Rückkehr zur Ladestation</li>
 * <li>89 ... Sörung des Laserdistanzsensors LDS. Versuchen Sie auf den LDS zu tippen.</li>
 * <li>90 ... Aktualisierung wird durchgeführt. Bitte warten.</li>
 * <li>... bis 110 weiter nichts</li>
 * </ul>
 */
public class PlaySoundPacket extends ActionPacket {
    public static final String ACTION = "PlaySound";

    public static final String ATTRIBUTE_SOUND_ID = "sid";

    private int soundId;

    public PlaySoundPacket(String ctlStanzaId, int soundId) {
        super(ctlStanzaId, true, false);

        setSoundId(soundId);
    }

    public PlaySoundPacket(Jid from, Jid to, int soundId) {
        super(from, to, true, false);

        setSoundId(soundId);
    }

    public int getSoundId() {
        return soundId;
    }

    public void setSoundId(int soundId) {
        this.soundId = soundId;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> additionalAttributes = new HashMap<>(1);
        additionalAttributes.put(ATTRIBUTE_SOUND_ID, String.valueOf(soundId));

        return additionalAttributes;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
