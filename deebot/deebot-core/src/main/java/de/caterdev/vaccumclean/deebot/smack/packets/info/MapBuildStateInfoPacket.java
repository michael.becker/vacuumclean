/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info;

import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

public class MapBuildStateInfoPacket extends InfoPacket {
    public static final String INFO = "MapSt";

    public static final String ATTRIBUTE_MAP_STATUS = "st";
    public static final String ATTRIBUTE_MAP_METHOD = "method";
    public static final String ATTRIBITE_BUILD_INFO = "info";

    private final String mapStatus;
    private final String mapMethod;
    private final String buildInfo;

    public MapBuildStateInfoPacket(String ts, XmlPullParser parser) {
        super(ts, true, false,true);

        this.mapStatus = parser.getAttributeValue(null, ATTRIBUTE_MAP_STATUS);
        this.mapMethod = parser.getAttributeValue(null, ATTRIBUTE_MAP_METHOD);
        this.buildInfo = parser.getAttributeValue(null, ATTRIBITE_BUILD_INFO);
    }

    @Override
    protected String getInfo() {
        return INFO;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> attributes = new HashMap<>(3);
        attributes.put(ATTRIBUTE_MAP_STATUS, mapStatus);
        attributes.put(ATTRIBUTE_MAP_METHOD, mapMethod);
        attributes.put(ATTRIBITE_BUILD_INFO, buildInfo);

        return attributes;
    }
}
