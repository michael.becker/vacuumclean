/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.response.SimpleResponsePacket;
import lombok.Getter;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;

/**
 * <iq id='$id' to='bot' from='client' type='set'>
 * <query xmlns="com:ctl">
 * <ctl id='id' td='Clean'>
 * <clean type='auto' speed='standard' act='s'/>
 * </ctl>
 * </query>
 * </iq>
 * <p>
 * TODO: enable spotted cleaning using mid
 * TODO: enable user defined area cleaning
 */
public class CleanPacket extends ActionPacket<SimpleResponsePacket> {
    public static final String ACTION = "Clean";

    public static final String ELEMENT_CLEAN = "clean";

    public static final String ATTRIBUTE_TYPE = "type";
    public static final String ATTRIBUTE_SPEED = "speed";
    public static final String ATTRIBUTE_ACT = "act";
    public static final String ATTRIBUTE_MIDS = "mid";

    public static final String DEFAULT_TYPE = "auto";
    public static final String DEFAULT_SPEED = "standard";
    public static final String DEFAULT_ACT = "s";

    @Getter
    private final String cleanType;
    @Getter
    private final String speed;
    @Getter
    private final String act;
    @Getter
    private final String mids;

    public CleanPacket(Jid from, Jid to, String cleanType, String speed, String act, String mids) {
        super(from, to, false, true);

        this.cleanType = cleanType;
        this.speed = speed;
        this.act = act;
        this.mids = mids;
    }

    public CleanPacket(String ctlId, String cleanType, String speed, String act, String mids) {
        super(ctlId, false, true);

        this.cleanType = cleanType;
        this.speed = speed;
        this.act = act;
        this.mids = mids;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.halfOpenElement(ELEMENT_CLEAN);
        xml.attribute(ATTRIBUTE_TYPE, cleanType);
        xml.attribute(ATTRIBUTE_SPEED, speed);
        xml.attribute(ATTRIBUTE_ACT, act);

        if (null != mids) {
            xml.attribute(ATTRIBUTE_MIDS, mids);
        }

        xml.closeEmptyElement();

        return xml;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
