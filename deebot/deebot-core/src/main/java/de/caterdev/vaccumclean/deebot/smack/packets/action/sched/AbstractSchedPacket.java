/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action.sched;

import de.caterdev.vaccumclean.deebot.smack.packets.Query;
import de.caterdev.vaccumclean.deebot.smack.packets.action.ActionPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.action.CleanPacket;
import de.caterdev.vacuumclean.deebot.core.clean.CleanSchedule;
import de.caterdev.vacuumclean.deebot.core.constants.Day;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;
import org.xmlpull.v1.XmlPullParser;

public abstract class AbstractSchedPacket<T extends Query> extends ActionPacket<T> {
    public static final String ELEMENT_SETTINGS = "s";
    public static final String ATTRIBUTE_NAME = "n";
    public static final String ATTRIBUTE_ON_OFF = "o";
    public static final String ATTRIBUTE_HOUR = "h";
    public static final String ATTRIBUTE_MINUTE = "m";
    public static final String ATTRIBUTE_DAYS = "r";
    public static final String ATTRIBUTE_SOURCE = "f";

    private XmlPullParser parser;

    private String name;
    private String on;
    private String hour;
    private String minute;
    private String days;
    private String cleanType;

    public AbstractSchedPacket(String innerId, XmlPullParser parser) {
        super(innerId, false, true);

        this.parser = parser;
    }

    public AbstractSchedPacket(Jid from, Jid to, CleanSchedule cleanSchedule) {
        super(from, to, false, true);

        this.name = cleanSchedule.getName();
        this.on = cleanSchedule.isActive() ? "1" : "0";
        this.hour = String.valueOf(cleanSchedule.getHour());
        this.minute = String.valueOf(cleanSchedule.getMinute());
        this.days = Day.getEncodedWeek(cleanSchedule.getRepeatDays());
        this.cleanType = cleanSchedule.getCleanType().getType();
    }

    public static CleanSchedule parseContents(XmlPullParser parser) throws Exception {
        String name = parser.getAttributeValue(null, AbstractSchedPacket.ATTRIBUTE_NAME);
        String active = parser.getAttributeValue(null, AbstractSchedPacket.ATTRIBUTE_ON_OFF);
        String hour = parser.getAttributeValue(null, AbstractSchedPacket.ATTRIBUTE_HOUR);
        String minute = parser.getAttributeValue(null, ATTRIBUTE_MINUTE);
        String days = parser.getAttributeValue(null, ATTRIBUTE_DAYS);
        String source = parser.getAttributeValue(null, ATTRIBUTE_SOURCE);

        parser.nextTag();
        parser.nextTag();
        String cleanType = parser.getAttributeValue(null, CleanPacket.ATTRIBUTE_TYPE);

        return new CleanSchedule(name, active, hour, minute, days, cleanType, source);
    }

    public static XmlStringBuilder buildCleanSchedule(XmlStringBuilder xml, CleanSchedule cleanSchedule) {
        xml.halfOpenElement(ELEMENT_SETTINGS);
        xml.attribute(ATTRIBUTE_NAME, cleanSchedule.getName());
        xml.attribute(ATTRIBUTE_ON_OFF, cleanSchedule.isActive() ? "1" : "0");
        xml.attribute(ATTRIBUTE_HOUR, String.valueOf(cleanSchedule.getHour()));
        xml.attribute(ATTRIBUTE_MINUTE, String.valueOf(cleanSchedule.getMinute()));
        xml.attribute(ATTRIBUTE_DAYS, Day.getEncodedWeek(cleanSchedule.getRepeatDays()));
        xml.attribute(ATTRIBUTE_SOURCE, cleanSchedule.getCleanScheduleSource().getSource());
        xml.rightAngleBracket();

        xml.halfOpenElement(ELEMENT_CTL);
        xml.attribute(ATTRIBUTE_TD, CleanPacket.ACTION);
        xml.rightAngleBracket();

        xml.halfOpenElement(CleanPacket.ELEMENT_CLEAN);
        xml.attribute(CleanPacket.ATTRIBUTE_TYPE, cleanSchedule.getCleanType().getType());
        xml.closeEmptyElement();

        xml.closeElement(ELEMENT_CTL);
        xml.closeElement(ELEMENT_SETTINGS);

        return xml;
    }

    protected void parseContents() {
        try {
            parser.nextTag();

            this.name = parser.getAttributeValue(null, ATTRIBUTE_NAME);
            this.on = parser.getAttributeValue(null, ATTRIBUTE_ON_OFF);
            this.hour = parser.getAttributeValue(null, ATTRIBUTE_HOUR);
            this.minute = parser.getAttributeValue(null, ATTRIBUTE_MINUTE);
            this.days = parser.getAttributeValue(null, ATTRIBUTE_DAYS);

            parser.nextTag();
            parser.nextTag();
            this.cleanType = parser.getAttributeValue(null, CleanPacket.ATTRIBUTE_TYPE);
        } catch (final Exception e) {
            throw new RuntimeException("Could not parse SchedPacket with parser " + parser);
        }
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.halfOpenElement(ELEMENT_SETTINGS);
        xml.attribute(ATTRIBUTE_NAME, name);
        xml.attribute(ATTRIBUTE_ON_OFF, on);
        xml.attribute(ATTRIBUTE_HOUR, hour);
        xml.attribute(ATTRIBUTE_MINUTE, minute);
        xml.attribute(ATTRIBUTE_DAYS, days);
        xml.rightAngleBracket();

        xml.halfOpenElement(ELEMENT_CTL);
        xml.attribute(ATTRIBUTE_TD, CleanPacket.ACTION);
        xml.rightAngleBracket();

        xml.halfOpenElement(CleanPacket.ELEMENT_CLEAN);
        xml.attribute(CleanPacket.ATTRIBUTE_TYPE, cleanType);
        xml.closeEmptyElement();

        xml.closeElement(ELEMENT_CTL);
        xml.closeElement(ELEMENT_SETTINGS);


        return xml;
    }

}
