/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info;

import de.caterdev.vacuumclean.deebot.core.clean.CleanState;
import de.caterdev.vacuumclean.deebot.core.constants.CleanSpeed;
import de.caterdev.vacuumclean.deebot.core.constants.CleanStatus;
import de.caterdev.vacuumclean.deebot.core.constants.CleanStopReason;
import de.caterdev.vacuumclean.deebot.core.constants.CleanType;
import lombok.Getter;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;

/**
 * <iq to='$to' type='set' id='02C7802EA410'><query xmlns='com:ctl'><ctl ts='$ts' td='CleanReport'><clean type='$type' speed='$speed' st='$status' rsn='$stopReason' a='$area' l='$lastDistance' sts='$startTime'/></ctl></query></iq>
 */
public class CleanReportInfoPacket extends InfoPacket {
    public static final String INFO = "CleanReport";

    public static final String ELEMENT_CLEAN = "clean";
    public static final String ATTRIBUTE_CLEAN_TYPE = "type";
    public static final String ATTRIBUTE_CLEAN_SPEED = "speed";
    public static final String ATTRIBUTE_CLEAN_STATUS = "st";
    public static final String ATTRIBUTE_STOP_REASON = "rsn";
    public static final String ATTRIBUTE_AREA = "a";
    public static final String ATTRIBUTE_LAST_DISTANCE = "l";
    public static final String ATTRIBUTE_START_TIME = "sts";

    private final String cleanType;
    private final String cleanSpeed;
    private final String cleanStatus;
    private final String stopReason;
    private final String area;
    private final String lastDistance;
    private final String startTime;

    @Getter
    private final CleanState cleanState;

    public CleanReportInfoPacket(String innerId, XmlPullParser parser) throws Exception {
        super(innerId, false, true);

        parser.nextTag();
        this.cleanType = parser.getAttributeValue(null, ATTRIBUTE_CLEAN_TYPE);
        this.cleanSpeed = parser.getAttributeValue(null, ATTRIBUTE_CLEAN_SPEED);
        this.cleanStatus = parser.getAttributeValue(null, ATTRIBUTE_CLEAN_STATUS);
        this.stopReason = parser.getAttributeValue(null, ATTRIBUTE_STOP_REASON);
        this.area = parser.getAttributeValue(null, ATTRIBUTE_AREA);
        this.lastDistance = parser.getAttributeValue(null, ATTRIBUTE_LAST_DISTANCE);
        this.startTime = parser.getAttributeValue(null, ATTRIBUTE_START_TIME);

        int area = this.area.equals("") ? 0 : Integer.valueOf(this.area);
        int lastDistance = this.lastDistance.equals("") ? 0 : Integer.valueOf(this.lastDistance);
        long startTime = this.startTime.equals("") ? 0 : Integer.valueOf(this.startTime);

        // TODO: für andere Rückmeldungen anpassen (u.a. wenn cleanStartReason nicht null ist usw.)
        this.cleanState = new CleanState(null, CleanSpeed.get(cleanSpeed), CleanStatus.get(cleanStatus), CleanType.get(cleanType), null, null, CleanStopReason.get(stopReason), area, 0, lastDistance, null, null, null, null, startTime, 0);
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.halfOpenElement(ELEMENT_CLEAN);
        xml.attribute(ATTRIBUTE_CLEAN_TYPE, cleanType);
        xml.attribute(ATTRIBUTE_CLEAN_SPEED, cleanSpeed);
        xml.attribute(ATTRIBUTE_CLEAN_STATUS, cleanStatus);
        xml.attribute(ATTRIBUTE_STOP_REASON, stopReason);
        xml.attribute(ATTRIBUTE_AREA, area);
        xml.attribute(ATTRIBUTE_LAST_DISTANCE, lastDistance);
        xml.attribute(ATTRIBUTE_START_TIME, startTime);
        xml.closeEmptyElement();

        return xml;
    }

    @Override
    protected String getInfo() {
        return INFO;
    }
}
