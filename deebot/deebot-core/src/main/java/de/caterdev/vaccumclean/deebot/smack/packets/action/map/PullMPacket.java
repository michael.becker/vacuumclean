/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action.map;

import de.caterdev.vaccumclean.deebot.smack.packets.action.ActionPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.PullMResponsePacket;
import de.caterdev.vacuumclean.deebot.core.map.MapSetType;
import org.jxmpp.jid.Jid;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq id="$id" to="$to" from="$from" type="set"><query xmlns="com:ctl"><ctl id="$innerId" td="PullM" tp="$mapSetType" msid="$mapSetId" mid="$mapDetailId" seq="?$seq?"/></query></iq>
 */
public class PullMPacket extends ActionPacket<PullMResponsePacket> {
    public static final String ACTION = "PullM";

    public static final String ATTRIBUTE_MAP_SET_TYPE = "tp";
    public static final String ATTRIBUTE_MAP_SET_ID = "msid";
    public static final String ATTRIBUTE_MAP_DETAIL_ID = "mid";
    public static final String ATTRIBUT_SEQ = "seq";

    private final MapSetType mapSetType;
    private final int mapSetId;
    private final int mapDetailId;
    private final int seq;

    public PullMPacket(String innerId, XmlPullParser parser) {
        super(innerId, true, false);

        this.mapSetType = MapSetType.get(parser.getAttributeValue(null, ATTRIBUTE_MAP_SET_TYPE));
        this.mapSetId = Integer.parseInt(parser.getAttributeValue(null, ATTRIBUTE_MAP_SET_ID));
        this.mapDetailId = Integer.parseInt(parser.getAttributeValue(null, ATTRIBUTE_MAP_DETAIL_ID));
        this.seq = Integer.parseInt(parser.getAttributeValue(null, ATTRIBUT_SEQ));
    }

    public PullMPacket(Jid from, Jid to, MapSetType mapSetType, int mapSetId, int mapDetailId, int seq) {
        super(from, to, true, false);

        this.mapSetType = mapSetType;
        this.mapSetId = mapSetId;
        this.mapDetailId = mapDetailId;
        this.seq = seq;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> attributes = new HashMap<>(4);
        attributes.put(ATTRIBUTE_MAP_SET_TYPE, mapSetType.getType());
        attributes.put(ATTRIBUTE_MAP_SET_ID, String.valueOf(mapSetId));
        attributes.put(ATTRIBUTE_MAP_DETAIL_ID, String.valueOf(mapDetailId));
        attributes.put(ATTRIBUT_SEQ, String.valueOf(seq));

        return attributes;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
