/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response.clean;

import de.caterdev.vaccumclean.deebot.smack.packets.response.ResponsePacket;
import lombok.Getter;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok' a='$cleanedArea' l='$duration' c='$count'/></query></iq>
 */
public class GetCleanSumResponsePacket extends ResponsePacket {
    public static final String ATTRIBUTE_CLEANED_AREA = "a";
    public static final String ATTRIBUTE_DURATION = "l";
    public static final String ATTRIBUTE_COUNT = "c";

    @Getter
    private final int cleandArea;
    @Getter
    private final long duration;
    @Getter
    private final long count;

    public GetCleanSumResponsePacket(String innerId, XmlPullParser parser) {
        super(innerId, parser, true, false);

        this.cleandArea = Integer.valueOf(parser.getAttributeValue(null, ATTRIBUTE_CLEANED_AREA));
        this.duration = Long.valueOf(parser.getAttributeValue(null, ATTRIBUTE_DURATION));
        this.count = Long.valueOf(parser.getAttributeValue(null, ATTRIBUTE_COUNT));
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> attributes = new HashMap<>(3);
        attributes.put(ATTRIBUTE_CLEANED_AREA, String.valueOf(cleandArea));
        attributes.put(ATTRIBUTE_DURATION, String.valueOf(duration));
        attributes.put(ATTRIBUTE_COUNT, String.valueOf(count));

        return attributes;
    }
}
