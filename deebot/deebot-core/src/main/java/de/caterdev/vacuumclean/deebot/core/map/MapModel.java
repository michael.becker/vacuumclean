/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.map;

import lombok.Data;

import java.util.Arrays;
import java.util.zip.CRC32;

/**
 * A map model describes the underlying map of the robot.
 * <p>
 * Each map model consists of a specific amount of pieces which is determined by the multiplication of {@link #columnPiece} times {@link #rowPiece}. The checksums of these pieces are stored in {@link #crc}.
 */
public @Data
class MapModel {
    private final int mapId;
    private final int pixelWidth;
    private final int columnGrid;
    private final int columnPiece;
    private final int rowGrid;
    private final int rowPiece;
    private final long[] crc;
    private final long crcValueEmptyData;

    private final int pieceDataSize;
    private final int dataSize;

    private Position boxBottomRight;
    private Position boxTopLeft;

    public MapModel(String mapId, String columnGrid, String rowGrid, String columnPiece, String rowPiece, String pixelWidth, String crc) {
        this.mapId = Integer.valueOf(mapId);
        this.columnGrid = Integer.valueOf(columnGrid);
        this.rowGrid = Integer.valueOf(rowGrid);
        this.columnPiece = Integer.valueOf(columnPiece);
        this.rowPiece = Integer.valueOf(rowPiece);
        this.pixelWidth = Integer.valueOf(pixelWidth);

        this.crc = Arrays.stream(crc.split(",")).mapToLong(Long::valueOf).toArray();

        this.pieceDataSize = this.rowGrid * this.columnGrid;
        this.dataSize = this.rowGrid * this.columnGrid * this.rowPiece * this.columnPiece;

        CRC32 crc32 = new CRC32();
        crc32.update(new byte[pieceDataSize]);
        this.crcValueEmptyData = crc32.getValue();

    }

    public boolean hasData(int pieceNumber) {
        if (pieceNumber >= crc.length) {
            throw new RuntimeException(String.format("MapModel contains only %s pieces\n", crc.length));
        }

        return crc[pieceNumber] != crcValueEmptyData;
    }
}