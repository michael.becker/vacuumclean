/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.clean;

import de.caterdev.vacuumclean.deebot.core.Constants;
import de.caterdev.vacuumclean.deebot.core.constants.CleanType;
import de.caterdev.vacuumclean.deebot.core.constants.Day;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class CleanSchedule {
    private final String name;
    private boolean active;
    private int hour;
    private int minute;
    private List<Day> repeatDays;
    private CleanType cleanType;
    private CleanScheduleSource cleanScheduleSource;

    public CleanSchedule() {
        this.name = "schedule_" + StanzaIdUtil.newStanzaId();
        this.repeatDays = new ArrayList<>();
    }

    public CleanSchedule(String name, String active, String hour, String minute, String repeatDays, String cleanType, String cleanScheduleSource) {
        this.name = name;
        this.active = active.equals("1");
        this.hour = Integer.valueOf(hour);
        this.minute = Integer.valueOf(minute);
        this.repeatDays = Day.parseEncodedWeek(repeatDays);
        this.cleanType = CleanType.get(cleanType);
        this.cleanScheduleSource = CleanScheduleSource.get(cleanScheduleSource);
    }

    @AllArgsConstructor
    public enum CleanScheduleSource {
        App("p"), Device("q"), IR("i");

        @Getter
        private final String source;

        public static CleanScheduleSource get(String value) {
            for (CleanScheduleSource cleanScheduleSource : values()) {
                if (cleanScheduleSource.source.equals(value)) {
                    return cleanScheduleSource;
                }
            }

            throw new RuntimeException(String.format(Constants.ENUM_DOES_NOT_EXIST, value, CleanScheduleSource.class, Arrays.toString(values())));
        }

    }
}
