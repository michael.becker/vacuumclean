/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core;

import de.caterdev.vacuumclean.core.crc.CRC;
import de.caterdev.vacuumclean.core.crc.CRC8;
import de.caterdev.vacuumclean.deebot.core.map.TracePoint;
import lombok.extern.java.Log;
import lzma.sdk.lzma.Decoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@Log
public class DataParseUtils {
    public static final byte LEN_DECODER_PROPERTIES = 5;
    public static final byte LEN_DATA_SIZE = 4;
    public static final byte[] DECODER_PROPERTIES = {93, 0, 0, 4, 0};

    public static final byte LEN_TRACEPOINT_FIELD = 5;

    public static byte[] decode7zData(String s) throws Exception {
        final byte[] fromBase64 = Base64.getDecoder().decode(s);
        if (fromBase64 == null) {
            throw new RuntimeException("Could not base64-decode map data from " + s);
        } else if (fromBase64.length <= (LEN_DECODER_PROPERTIES + LEN_DATA_SIZE)) {
            throw new RuntimeException(Arrays.toString(fromBase64) + " does not contain enough data to encode a map");
        } else {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(fromBase64);

            final byte[] decoderProperties = new byte[LEN_DECODER_PROPERTIES];
            byteArrayInputStream.read(decoderProperties);

            for (int i = 0; i < DECODER_PROPERTIES.length; i++) {
                if (decoderProperties[i] != DECODER_PROPERTIES[i]) {
                    throw new RuntimeException("decoder properties " + Arrays.toString(decoderProperties) + " do not match expected properties " + Arrays.toString(DECODER_PROPERTIES));
                }
            }

            final byte[] dataSize = new byte[LEN_DATA_SIZE];
            byteArrayInputStream.read(dataSize);
            final int dataLength = toInt(dataSize, 0);

            // TODO: Fehler wenn properties/code nicht funktionieren
            Decoder decoder = new Decoder();
            boolean properties = decoder.setDecoderProperties(decoderProperties);
            boolean code = decoder.code(byteArrayInputStream, byteArrayOutputStream, dataLength);

            return byteArrayOutputStream.toByteArray();
        }
    }

    /**
     * Decodes a given trace point String.
     * <p>
     * Each trace point String is decoded to raw bytes by calling {@link #decode7zData(String)}. A single trace point byte representation should consist of five bytes. The first two bytes represent x-position (type short), the second to bytes represent the y-position (type short). The last byte defines whether the trace point is connected with the previous point and the type of the trace point.
     * </p>
     * <p>
     * TODO: connected with previous = 1 has not been seen so far, interpretation of type ist not clear
     * </p>
     *
     * @param encodedTracePoints
     * @return
     * @throws Exception
     */
    public static List<TracePoint> parseTracePoints(String encodedTracePoints) throws Exception {
        final byte[] decodedTracePoints = DataParseUtils.decode7zData(encodedTracePoints);
        List<TracePoint> tracePoints = new ArrayList<>();

        for (int i = 0; i < decodedTracePoints.length; i += LEN_TRACEPOINT_FIELD) {
            short positionX = toShort(decodedTracePoints, i);
            short positionY = toShort(decodedTracePoints, i + Short.BYTES);
            byte type = decodedTracePoints[i + Short.BYTES + Short.BYTES];
            boolean connectedWithPrevious = (decodedTracePoints[i + Short.BYTES + Short.BYTES] >>> 7) != 0; // TODO: klären


            TracePoint tracePoint = new TracePoint(positionX, positionY, connectedWithPrevious, type);
            tracePoints.add(tracePoint);
        }

        return tracePoints;
    }

    private static int toInt(byte[] bytes, int start) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        return buffer.getInt(start);
    }

    public static short toShort(byte[] bytes, int start) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        return buffer.getShort(start);
    }

    /**
     * Returns the configuration byte representation of the given string. Used for transferring initial settings to the bot during wifi configuration.
     *
     * @param paramString
     * @return
     */
    public static byte[] getConfigBuffer(String paramString) {
        byte[] arrayOfByte1 = paramString.getBytes();
        int i = arrayOfByte1.length;
        byte[] arrayOfByte2 = new byte[arrayOfByte1.length + 4];

        CRC crc = new CRC8();
        crc.update(arrayOfByte1);

        // TODO: what doe these values mean?
        arrayOfByte2[0] = 66;
        arrayOfByte2[1] = -92;
        arrayOfByte2[2] = 89;
        arrayOfByte2[3] = (byte) crc.getValue();

        System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 4, i);
        return arrayOfByte2;
    }

}
