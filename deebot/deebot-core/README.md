# General System Architecture
## Robot-Server-Client communication
The Deebot Server consists of a RobotServer for propagation of communication endpoints and an XMPP server for sending and receiving [XMPP messages](https://xmpp.org/).

## Establishing connection to a new device

## XMPP Messages
A proprietary extension of [XMPP query action protocol](https://xmpp.org/extensions/xep-0099.html) is used for sending commands from the server to the robot and receiving responses to these commands. The general format of the messages is `<iq id='$id' to='$to' from='$from' type='$type'><query xmlns='com:ctl'><ctl id='$innerId' [attributes]>[elements]</ctl></query></iq>`. It is possible to distinguish between three different kinds of XMPP packet:
* Actions are used to send commands from the server to the robot. Each action has an additional `td` attribute in the `ctl` element representing the specific command, e.g. `GetBatteryInfo` or `GetChargeState`. Several actions also have additional encapsulated elements.
* Responses are sent from the robot to the server as a response to action. Matching between actions and responses is done by using the attribute `innerId`. All (**TODO: check**) response packets have an additional `ret` attribute in their `ctl` element. If everything works fine the value of this attribute is `ok`.
* Infos are sent from the robot to the server to give information about currently executed activities or about the robot status, e.g. the position of the robot during movement. Every info packet has an additional `td` attribute in its `ctl` element to represent the information, e.g. `BatteryInfo` or `Pos`. In addition, many of the info packets also have an attribute `ts`. The meaning of this attribute is as of yet not known. However, the `ts`-value does not seem to have an impact on the received information/status.

# Actions
The bot receives actions via ActionPacket XMPP packets. Each packet usually consists of a `td` attribute representing the activity. In addition, an action is followed by a response from the bot, e.g. to return status information or results of actions.

## Supported Actions
### Map Administration

#### Map Model
A map model describes the underlying map of the bot.

##### Action Packet: GetMapModelPacket
`<iq id="$id" to="$bot" from="$client" type="set"><query xmlns="com:ctl"><ctl id="$innerId" td="GetMapM"/></query></iq>`

##### Response Packet: GetMapModelResponsePacket
`<iq to='$user' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' i='$mapId' w='$columnGrid' h='$rowGrid' r='$rowPiece' c='$columnPiece' p='$pixelWidth' m='$crc[]'/></query></iq>`

#### Map Pieces
Each bot map consists of a specific amount of pieces. Using the action/response packets, it is possible to pull the map from the robot.

##### Action Packet: PullMapPiecePacket
`<iq id="$id" to="$bot" from="$client" type="set"><query xmlns="com:ctl"><ctl id="$innerId" td="PullMP" pid="$pieceId"/></query></iq>`

##### Response Packet: PullMapPieceResponsePacket
`<iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='$retValue' i='$mapId' p='$data'/></query></iq>`

#### Spot Areas and Virtual Walls
##### Action Packet: GetMapSetPacket
`<iq to='$to' from='$from' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='GetMapSet' tp='$mapSetType'/></query></iq>`

##### Response Packet: GetMapSetResponsePacket
`<iq to='$tp' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok' tp='$mapSetType' msid='$mapSetId'><m mid='0' p='1'/><m mid='1' p='1'/><m mid='2' p='1'/><m mid='3' p='1'/><m mid='4' p='1'/><m mid='5' p='1'/></ctl></query></iq>`

##### Action Packet: PullMPacket
`<iq id="$id" to="$to" from="$from" type="set"><query xmlns="com:ctl"><ctl id="$innerId" td="PullM" tp="$mapSetType" msid="$mapSetId" mid="$mapDetailId" seq="?$seq?"/></query></iq>`

Using the PullMPacket, spot areas (using *tp*-value *sa*) and virtual walls (using *tp*-value *vw*) can be pulled from the server.

##### Response Packet: PullMResponsePacket
`<iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok' m='$data'/></query></iq>`


### Block Times
Block Times are used to set times in which the bot is not allowed to clean.

#### Get Block Times
##### Action Packet: GetBlockTimePacket
`<iq xmlns='jabber:client' to='$bot' from='$user' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='GetBlockTime'/></query></iq>`

##### Response Packet: GetBlockTimeResponsePacket
`<iq xmlns='jabber:client' to='$user' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' ret='$ret'><bt sh='$startHour' sm='$startMinute' eh='$endHour' em='$endMinute'/></ctl></query></iq>`

**TODO: check, if multiple block times are possible**

### Charger Position
#### Get Charger Position
##### Action Packet: GetChargerPosPacket
`<iq to='$bot' from='$user' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='GetChargerPos'/></query></iq>`

##### Response Packet: GetPosResponsePacket
`<iq to='$user' type='set' id='$id'><query xmlns='com:ctl'><ctl id='%innerId' ret='ok' p='$position' a='$angle'/></query></iq>` 

### Bot Position
#### Get Bot Position
##### Action Packet: GetPosPacket
`<iq to='$bot' from='$user' id='id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='GetPos'/></query></iq>`

##### Response Packet: GetPosResponsePacket
`<iq to='$user' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok' p='$position' a='$angle'/></query></iq>`

### Scheduled Cleaning
#### Get Cleaning Schedule
##### Action Packet: GetSchedPacket
`<iq id='$id' to='$bot' from='$client' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='GetSched'/></query></iq>`

##### Response Packet: GetSchedResponsePacket
`<iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok'><s n='$name' o='$active' h='$hour' m='$minute' r='$days' f='$source'><ctl td='Clean'><clean type='$cleanType'/></ctl></s></ctl></query></iq>`

The response to the GetSchedPacket contains als currently existing scheduled cleanings. Every scheduled cleaning is encapsulated in `<s ...></s>` elements and has the following attributes:
* name: A unique name. The original app seems to generate names with the prefix `timing_name_` and a random number. However, the name does not have any impact on the schedule itself. Thus, an arbitrary name can be assigned.
* active: A scheduled can either be active (represented by `1` in the respective XML element) or inactive (`0`).
* hour: The hour at which the cleaning shall start.
* minute: The minute at which the cleaning shall start.
* days: The days parameter is used to enable repeated cleanings. It consists of seven digits each having a value of `0` or `1`. Each digit represents a day of the week starting with Sunday. Thus, a scheduled cleaning that should be repeated every Monday and Wednesday has encoded days as `0101000`. The enum `Day` provides methods for encoding and decoding days.
* source: Each schedule has a designated source. Currently, sources `p` (representing the app) and `q` representing the robot were seen. However, there seems to be another source IR represented by `i`. As of yet, it is not known whether the source plays any role in executing a scheduled cleaning.

#### Modify Cleaning Schedule
##### Action Packet: AddSchedPacket
`<iq id="$id" to="$bot" from="$client" type="set"><query xmlns="com:ctl"><ctl id="$innerId" td="AddSched"><s n="$name" o="$active" h="$hour" m="$minute" r="$days"><ctl td="Clean"><clean type="auto"/></ctl></s></ctl></query></iq>`

For adding a new scheduled cleaning the XML element `s` with its attributes is used. A detailed descriptions of these elements is presented in [Get Cleaning Schedule](#get-cleaning-schedule). The cleaning action is encapsulated inside the `s` element. Currently, only auto cleaning is supported. However, it should be possible to implement other cleaning types, too.

**TODO: It may also be possible to send other commands than cleaning - try this.** 

##### Action Packet: ModSchedPacket
`<iq id='$id' to='$to' from='$from' type='set'><query xmlns='com:ctl'><ctl id='81718615' td='ModSched'><ModSched n='$name'><s n='$newName' o='$active' h='$hour' m='$minute' r='$days'><ctl td='Clean'><clean type='auto'/></ctl></s></ModSched></ctl></query></iq>`

Modifying a scheduled cleaning has similar attributes as adding a modified cleaning. The only difference is the additional `ModSched` element containing the name of the cleaning to modify.

##### ActionPacket: DeleteSchedPacket
`<iq id='$id' to='$to' from='$from' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='DelSched'><DelSched n='$name'/></ctl></query></iq>`

A scheduled cleaning can be deleted by providing its name to the packet.

### Charge State
#### Get Charge State
##### Action Packet: GetChargeStatePacket
`<iq id="$id" to="$robot" from="$client" type="set"><query xmlns="com:ctl"><ctl id="$ctlId" td="GetChargeState"/></query></iq>`

##### Response Packet: GetChargeStateResponsePacket
`<iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='$ret'><charge type='$chargeStat'/></ctl></query></iq>`

Possible charge states are:
* Idle
* Going
* SlotCharging

### Language
#### Get Existing Languages
* GetLanguages

#### Get Active Language
##### Action Packet: GetActiveLanguagePacket
`<iq to='$bot' from='$user' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='GetActiveLanguage'/></query></iq>`
##### Response Packet: GetActiveLanguageResponsePacket
**TODO**
### Set Active Language

### Battery Info
#### Get Battery Info
##### Action Packet: GetBatteryInfoPacket
`<iq id="$ID" to="$BOT" from="$USER" type="set"><query xmlns="com:ctl"><ctl id="$ID" td="GetBatteryInfo"/></query></iq>`
##### Response Packet: GetBatteryInfoResponsePacket
`<iq to='$receiver' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok'><battery power='$power'/></ctl></query></iq>`
 

## Additional Actions (not implemented yet)
* GetCleanLogs (count)
* GetCleanSpeed
* GetCleanState
* GetCleanStatistics
* GetCleanSum
* GetError
* GetLifeSpan (type)
* GetLogs (count)
* GetMapSet (tp)
* GetMapSt
* GetMute
* GetOnOff (t)
* GetProgress (act)
* GetSilentModeState
* GetSleepStatus
* GetSpotArea
* GetTime


* SetActiveLanguage (l)
* SetBlockTime (<bt sh,sm,eh,em/>)
* SetMute (mute=1,0)
* SetOnOff (t=?, on=1/0)
* SetVolume (<volume unit="percentage" val=x)

* GetMapMResponsePacket

# Map details
Using the `GetMapModelPacket` action, the bot returns a `GetMapModelResponsePacke` which contains metadata of the underlying map stored at the bot. The response packet looks as follows:

`<iq to='$user' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' i='$mapId' w='$columnGrid' h='$rowGrid' r='$rowPiece' c='$columnPiece' p='$pixelWidth' m='$crc[]'/></query></iq>`.

Each Deebot map identified by `mapId` consists of pieces to separate parts of the map. The amount of pieces is determined by the multiplication of `rowPiece` times `columnPiece`. The array `crc[]` contains the checksums of all pieces as defined by the [CRC32 algorithm](https://docs.oracle.com/javase/8/docs/api/java/util/zip/CRC32.html). This allows for an efficient identification of changed map pieces.

A map piece contains `rowGrid * columnGrid` bits to represent the map data. **The meaning of pixelWidth is currently not known but seems to be relevant for scaling the map**

Take the following map model as an example: `MapModel(mapId=1675619197, pixelWidth=50, columnGrid=100, columnPiece=8, rowGrid=100, rowPiece=8, crc=[1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 405855736, 303969177, 369753284, 3855924242, 1295764014, 1295764014, 1295764014, 1295764014, 815663441, 2763375989, 1263960677, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 3480335780, 85136059, 3084431172, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014, 1295764014], boxBottomRight=null, boxTopLeft=null)
`. This map model defines a map consisting of 64 map pieces with each map piece having 10,000 bits to represent the map data. For each map piece the crc values are given where `1295764014` is the checksum for all zeros, i.e. the bot does not have map data for these pieces (yet).

Using the information from the map model we can retrieve a concrete map piece using the action `PullMapPiecePacket` with its response `PullMapPieceResponsePacket` which looks as follows: `<iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='$retValue' i='$mapId' p='$data'/></query></iq>`.

Unfortunately, the map piece response does not provide the piece Id so we have to remember this value when sending the packet. The important part is the `data` attribute which contains a [Base64](https://en.wikipedia.org/wiki/Base64) encoded array of bytes. The decoded bytes contain the [LZMA encoded](https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Markov_chain_algorithm) map data for this piece. The first five entries of the array are used as LZMA decoder properties and are always `93, 0, 0, 4, 0`. (**TODO: how are these values evaluated?**) The next four bytes represent an integer value to define the amount of bits used to represent the map data. It should be the same value as the amount determined by `rowGrid * columnGrid` from above. The remaining bytes contain the LZMA encoded map data.

For example, our map piece contains the following data: `XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4kijmo4YH+e7kHoLTL8U6PAFLsX7Jhrz0KgA=`. This string is decoded using the Base64 algorithm to `[93, 0, 0, 4, 0, 16, 39, 0, 0, 0, 0, 111, -3, -1, -1, -93, -73, -1, 71, 62, 72, 21, 114, 57, 97, 81, -72, -110, 40, -26, -93, -122, 7, -7, -18, -28, 30, -126, -45, 47, -59, 58, 60, 1, 75, -79, 126, -55, -122, -68, -12, 42, 0]`. As explained above, the first five bytes represent LZMA properties, i.e the array `[93, 0, 0, 4, 0]`. The next four bytes of the array `[16, 39, 0, 0]` are the byte representation of the size and converts to 10,000 (using little endian order). Finally, the remaining bytes (starting from `[0,0, 111, -3, ...]` are decoded using LZMA into a byte array of length 10,000.

The decoded map data can contain the following values:
* `0` represents no data
* `1` represents floor
* `2` represents a wall
* `3` represents carpet

To display map data the one-dimensional array of bytes containing the map piece information needs to be transformed into a two dimensional array. The first dimension of the array is defined by the product of `rowGrid * rowPiece` (height), the second dimension is the product of `columnGrid * columnPiece` width. It should have the same value as the amount of pieces times the amount of bytes to represent each piece. For the example above, we would have a height of 100 * 8 = 800 and a width of 100 * 8 = 800. The multiplication of height times width = 800 * 800 = 64,000 equals to the number of pieces (64) times the number of bytes to represent each piece (10,000).

An example for a grid layout is displayed in the image below. The underlying map model is defined as follows:
* columnGrid = 20
* rowGrid = 20
* columnPiece = 3
* rowPiece =3

![gridlayout](gridlayout.PNG) 

Based on these values, the following properties are calculated:
* pieces = columnPiece * rowPiece = 3*3 = 9
* pieceSize = columnGrid * rowGrid = 20*20 = 400
* rows = rowGrid * rowPiece = 20*3 = 60
* columns = columnGrid * columnPiece = 20*3 = 60

We can see that our constraints hold: `pieceSize * pieces = 400*9 = 3600 = rows * columns = 60*60.`


The starting points in the two dimensional array for a piece are calculated as follows
* rowStart = pieceId / rowPiece
* columnStart = pieceId % columnPiece

To fill the buffer, the following loop is executed:

```
for (int row = 0; row < row_grid; row++) {
    for (int column = 0; column < column_grid; column++) {
        int bufferRow = row + rowStart * row_grid;
        int bufferColumn = column + columnStart * column_grid;
        int pieceDataPosition = row_grid * row + column;
        
        this.buffer[bufferRow][bufferColumn] = pieceData[pieceDataPosition];
    }
}
```

If we have the map model from above with a rowPiece/columnPiece of 8 and a rowGrid/columnGrid of 100, we can determine the buffer positions for all 64 map pieces. For example, the map piece with Id 0 would start from `rowStart = 0/8 = 0` and `columnStart = 0%8 = 0` The first entries are located at `[bufferRow = 0+0*100][bufferColumn = 0+0*100]`. The first row ends at `[bufferColumn = 99 + 0*100]`; the second row continues at `[bufferRow = 1 + 0*100][bufferColumn = 0 + 0*100]`. The last data is written into `[bufferRow = 99 + 0*100][bufferColumn = 99 + 0*199]`. The map piece with Id 1 starts at `rowStart = 0/8 = 0` and `columnStart = 0%8 = 1`. Thus, the first entry is located at `[0+0*100][0+1*100]`.

As can be seen from the calculations, every piece has the size 100x100:
* Piece 0 spans from `[0][0]` to `[99][99]`
* Piece 1 spans from `[0][100]` to `[99][199]`
* Piece 7 spans from `[0][700]`  to `[99,799]`
* Piece 8 spans from `[100][0]` to `[199][99]`
* Piece 63 spans from `[700][700]` to `[799][799]`

Using the map data, it is possible to display a map in the dimension rowGrid times columnGried, i.e. 800x800 for the example given above. The following code is used to display this map using a HTML canvas (see also method writeUnscaled in class MapInfo):
```
FileWriter writer = new FileWriter("map.html");
        writer.write("<!DOCTYPE html><html><head><title>Simple Canvas Example</title><style>canvas {border: 3px #CCC solid;}  </style></head><body><div id=\"container\"><canvas id=\"myCanvas\" height='" + (height) + "' width='" + (width) + "'></canvas></div><script>var mainCanvas = document.querySelector(\"#myCanvas\");var mainContext = mainCanvas.getContext(\"2d\");var canvasWidth = mainCanvas.width;var canvasHeight = mainCanvas.height;function drawCircle() {mainContext.clearRect(0, 0, canvasWidth, canvasHeight);\n");
        byte type = -1;
        for (int i = 0; i < mapInfo.getBuffer().length; i++) {
            for (int j = 0; j < mapInfo.getBuffer()[i].length; j++) {
                byte mapBuffer = mapInfo.getBuffer()[i][j];

                if (mapBuffer == MapInfo.NODATA && type != MapInfo.NODATA) {
                    continue;
                }

                if (mapBuffer == MapInfo.WALL && type != MapInfo.WALL) {
                    writer.write("mainContext.fillStyle = '#AAAAAA';\n");
                    type = MapInfo.WALL;
                } else if (mapBuffer == MapInfo.FLOOR && type != MapInfo.FLOOR) {
                    writer.write("mainContext.fillStyle = '#FFFFAA';\n");
                    type = MapInfo.FLOOR;
                } else if (mapBuffer == MapInfo.CARPET && type != MapInfo.CARPET) {
                    writer.write("mainContext.fillStyle = '#000000';\n");
                    type = MapInfo.CARPET;
                }

                writer.write("mainContext.fillRect(" + i + "," + j + "," + 1 + "," + 1 + ");\n");

            }
        }
        writer.write("mainContext.closePath();}drawCircle();</script></body></html>");
        writer.close();
```

For a more suitable representation, the map has to be scaled according to the desired screen size. Scaling is calculated in `CleanMapData#update(float width, float height)`.


TODO

## Spot Areas
Spot areas represent the division of the map by the robot. Using the spot area, it is possible to identify single rooms and, thus, start the cleaning at this specific room. To retrieve the spot areas from the robot, the following steps are necessary
### Retrieve the map set for the spot areas
 Retrieving the map set is done using the `GetMapSetPacket` (`<iq to='$to' from='$from' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='GetMapSet' tp='$mapSetType'/></query></iq>`) with `MapSetType#SpotArea`. The robot returns as a result the following response packet: `<iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok' m='-3300,-550;-3300,-350;-3150,-100;-3150,300;-2950,450;-3050,550;-3000,1100;-2800,1250;-2950,1400;-2600,2250;-2650,3350;-2550,3700;-1550,3650;-1450,3550;-1550,3200;-1300,2850;-1100,3150;-1250,3300;-1250,3650;-1050,3850;-400,3600;450,3600;850,3400;850,3100;650,2800;750,2700;1100,2700;1100,-1050;850,-1050;400,-850;100,-900;-100,-750;-700,-750;-1050,-550;-1200,-550;-1450,-800;-1450,-950;-2500,-550;-3300,-550'/></query></iq>`

The response contains the boundaries of the spot area as positions separated by `;`. That means, the first position is `[-3300,-550]` and so on. As can be seen from the result, the last position is the same as the first position and, thus, allows for drawing a polygon. The polygon represented by the above positions is displayed below:

![spotarea](spotarea.png) 
