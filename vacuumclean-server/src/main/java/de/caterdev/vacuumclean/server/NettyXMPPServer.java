/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.Getter;
import lombok.extern.java.Log;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Log
public abstract class NettyXMPPServer<U extends NettyXMPPClient> implements Runnable {
    public static final String LOG_MESSAGE_STARTING_SERVER = "Starting %s server at %s\n";
    public static final String LOG_MESSAGE_SHUTDOWN = "Shutting down %s server at %s\n";

    private static final int PORT = 5223;

    private final EventLoopGroup bossGroup = new NioEventLoopGroup();
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();
    private final SocketAddress socketAddress;

    @Getter
    private final Collection<Jid> botServerJids;
    @Getter
    private final Collection<Jid> appServerJids;

    private List<U> clients = Collections.synchronizedList(new ArrayList<>());

    protected NettyXMPPServer(String[] botServerJids, String[] appServerJids, SocketAddress socketAddress) throws Exception {
        this.botServerJids = new ArrayList<>(botServerJids.length);
        for (String botServerJid : botServerJids) {
            this.botServerJids.add(JidCreate.from(botServerJid));
        }

        this.appServerJids = new ArrayList<>(appServerJids.length);
        for (String appServerJid : appServerJids) {
            this.appServerJids.add(JidCreate.from(appServerJid));
        }
        this.socketAddress = socketAddress;
    }

    public void run() {

        try {
            log.info(String.format(LOG_MESSAGE_STARTING_SERVER, getClass().getSimpleName(), PORT));


            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup);
            b.channel(NioServerSocketChannel.class);
            b.option(ChannelOption.SO_BACKLOG, 128);
            b.childOption(ChannelOption.SO_KEEPALIVE, true);
            b.handler(new LoggingHandler(LogLevel.WARN));

            b.childHandler(new NettyXMPPChannelInitialiser<>(this));

            b.bind(socketAddress).sync().channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            shutdown();
        }
    }

    public void shutdown() {
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
        log.info(String.format(LOG_MESSAGE_SHUTDOWN, getClass().getSimpleName(), PORT));
    }

    public List<U> getClients() {
        return clients;
    }

    public U getClient(Jid jid) {
        for (U client : clients) {
            if (null != client.getClientJid() && client.getClientJid().equals(jid) || client.getClientJid().asBareJid().equals(jid)) {
                return client;
            }
        }

        return null;
    }

    public U getClient(String name) {
        for (U client : clients) {
            if (null != client.getClientName() && client.getClientName().equals(name)) {
                return client;
            }
        }

        return null;
    }

    public List<U> getUserClients() {
        List<U> userClients = new ArrayList<>();
        for (U client : clients) {
            if (!client.isRobot()) {
                userClients.add(client);
            }
        }

        return userClients;
    }

    public List<U> getRobotClients() {
        List<U> robotClients = new ArrayList<>();
        for (U client : clients) {
            if (client.isRobot()) {
                robotClients.add(client);
            }
        }

        return robotClients;
    }

    public U getRobotClient() {
        for (U client : clients) {
            if (client.isRobot()) {
                return client;
            }
        }

        return null;
    }

    protected abstract U getClientInstance();
}
